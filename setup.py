import os
from setuptools import setup, find_packages


requirements_path = os.path.join(os.path.dirname(__file__), 'requirements.txt')
with open(requirements_path) as requirements_file:
    install_requires = [
        l.strip()
        for l in requirements_file
        if l and not l.startswith('#')
    ]


setup(
    name = "ieeg",
    version = "0.1",
    author = "Krzysztof Leszczynski",
    author_email = "kl319367@students.mimuw.edu.pl",
    description = ("Script for seizure analisis for mgr."),
    keywords = "eeg machine-learning seizure-prediction",
    url = "git@gitlab.com:cristo92/SeizurePrediction.git",
    packages=find_packages(),
    install_requires=install_requires,
)

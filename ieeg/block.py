from os import path

import numpy as np

from ieeg.database import Database
from ieeg.parseHeader import read_data
from ieeg.settings import get_path


class Block(object):
    def __init__(self, block, channels, sample_rate):
        """
        :param block:dict, keys are equal columns of block in Database
        :param channels:int
        :param sample_rate:int
        """
        D = Database()
        file_info = D.get_file(block["eeg_file"])

        head, data = read_data(path.join(get_path(), file_info["path"][1:], file_info["name"]))

        if sample_rate != 256:
            div = sample_rate / 256
            n, m = data.shape
            self.data = data[:, range(0, m, div)]
        else:
            self.data = data
        self.noffset = 0

    def length(self):
        n, m = self.data.shape
        return m - self.noffset

    def remaining_dataset(self):
        n, m = self.data.shape
        if m - self.noffset == 0:
            return None
        return self.data[:, self.noffset:]

    def dataset(self, need, ndarray):
        """
        :param need:seconds,int - amount of data needed for next block
        :param ndarray:np.ndarray - narray which are going to be concatenated
        :return: np.ndarray
        """
        X = self.data[:, self.noffset:(need + self.noffset)]
        if ndarray is not None:
            X = np.concatenate((X, ndarray), axis=1)
        self.noffset += need
        return (X, self.noffset)

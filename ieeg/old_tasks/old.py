import os
from glob import glob

import h5py
import numpy as np

from daftlearning.workflow.workflow_tasks import WorkflowTask
from daftlearning.logger import logger
from ieeg.settings import get_path


###### OLD TASKS #######

class SaveAndReturn(WorkflowTask):
    def _run(self, X, Y, output_folder, output_filename, **kwargs):
        path = get_path()
        try:
            with h5py.File(os.path.join(output_folder, output_filename),
                           'w') as f:
                f.create_dataset('X', data=X)
                f.create_dataset('Y', data=Y)
            logger.info("saved to {}".format(output_folder + output_filename))
        except KeyboardInterrupt as e:
            logger.error("failed to save {}".format(output_folder + output_filename))
            raise e

        return {'X': X, 'Y': Y}


class LoadAndFeatureExtraction(WorkflowTask):
    def __init__(self, *args, **kwargs):
        super(LoadAndFeatureExtraction, self).__init__(*args, **kwargs)
        self.pipeline = None

    def _run(self, input_folder, feature_extraction_pipeline, **kwargs):
        if self.pipeline is None:
            self.pipeline = feature_extraction_pipeline


        path = get_path()
        folder = os.path.join(path, input_folder, '*.hdf5')

        features_matrix = []
        Y_matrix = []
        try:
            files = glob(folder)
            for i, file in enumerate(files):
                logger.info('Reading {}'.format(file))
                with h5py.File(file, 'r') as fdata:
                    X = np.array(fdata['X'])
                    Y = np.array(fdata['Y'])
                    features = self.pipeline.fit_transform(X, Y)
                    logger.info('Processing {}/{} file. From input shape {} we'
                                 ' got output shape: {}'.format(i + 1,
                                 len(files), X.shape, features.shape))
                    features_matrix.append(features)
                    Y_matrix.append(Y)
        except KeyboardInterrupt:
            logger.info('\nKeyboard Interrupt.')
            logger.info('Preprocessed {}/{} files'.format(i, len(files)))
            exit(1)

        return {
            'X': np.concatenate(features_matrix),
            'pipeline': self.pipeline,
            'Y': np.concatenate(Y_matrix),
        }


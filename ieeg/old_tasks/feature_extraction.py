import os
import h5py

from daftlearning.workflow import Workflow
from daftlearning.workflow.workflow_tasks import WorkflowTask
from daftlearning.logger import logger
from ieeg.old_tasks.old import LoadAndFeatureExtraction
from ieeg.data import MetaData

def fextraction_workflow(params):
    return (
        Workflow(params, 'Workflow for feature extraction')
        >> LoadAndFeatureExtraction('feature_extraction')
    )


class FeatureExtraction(WorkflowTask):
    def _run(self, args, in_folder, windows, *largs, **kwargs):
        out_folder = os.path.join(args.output, 'features')
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)
        for pid in args.pids:
            out_path_and_file = os.path.join(out_folder, '{}.hdf5'.format(pid))
            if os.path.exists(out_path_and_file):
                logger.info('Features: {} exists. Skipping feature_extraction.'\
                    .format(out_path_and_file))
                continue
            workflow = fextraction_workflow({
                'global_params': {'metadata': MetaData(pid)},
                'feature_extraction': {'input_folder': in_folder,
                                       'windows': windows}
            })

            D = workflow.run()
            X = D['X']
            Y = D['Y']
            logger.info("We have {} Ys".format(np.sum(Y, dtype=int)))
            logger.info("X: {}".format(X.shape))
            logger.info("Y: {}".format(Y.shape))

            try:
                with h5py.File(out_path_and_file, 'w') as f:
                    f.create_dataset('X', data=X)
                    f.create_dataset('Y', data=Y)
                    logger.info("saved to {}".format(out_path_and_file))
            except KeyboardInterrupt as e:
                logger.error("failed to save {}".format(out_path_and_file))
                raise e

        return {'in_folder': out_folder}
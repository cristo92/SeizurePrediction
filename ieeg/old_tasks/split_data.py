import os
import h5py

import numpy as np
from sklearn.model_selection import train_test_split

from daftlearning.workflow.workflow_tasks import WorkflowTask
from daftlearning.logger import logger


class SplitData(WorkflowTask):
    def _run(self, in_folder, test_size, **kwargs):
        X = []
        Y = []
        for file in os.listdir(in_folder):
            with h5py.File(os.path.join(in_folder, file)) as D:
                # TODO - should we really cast dataset to np.array?
                X.append(np.array(D['X']))
                Y.append(np.array(D['Y']))
        X = np.concatenate(X)
        Y = np.concatenate(Y).astype(int)
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size, stratify=Y)
        logger.info("Y in train: {}".format(np.sum(Y_train)))
        logger.info("Y in test: {}".format(np.sum(Y_test)))

        return {
            'X_train': X_train,
            'X_test': X_test,
            'Y_train': Y_train,
            'Y_test': Y_test
        }
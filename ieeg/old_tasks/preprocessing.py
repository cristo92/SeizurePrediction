import os

from daftlearning.workflow import Workflow
from daftlearning.workflow.workflow_tasks import WorkflowTask
from daftlearning.logger import logger
from ieeg.tasks.hypothesis import SurfaceEEG
from ieeg.old_tasks.old import SaveAndReturn
from ieeg.data import MetaData
from ieeg.data import Data
from ieeg.settings import get_path

def preprocessing_workflow(params):
    return (
        Workflow(params, 'Workflow for preprocessing raw data')
        >> SurfaceEEG('surface_eeg')
        >> SaveAndReturn('save_and_return')
    )

class PreprocessingTask(WorkflowTask):
    def _run(self, args, batch_size, observation_time, margin_time, *largs, **kwargs):
        out_folder = os.path.join(args.output, 'signals')
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)

        for pid in args.pids:
            has_finished_filename = os.path.join(out_folder,
                                                 '{}_success.bool'.format(pid))

            # Check if signals were already chosen
            if os.path.exists(has_finished_filename):
                logger.info("We have done preprocessing for {}. Skipping".format(
                    pid))
                continue

            workflow = preprocessing_workflow({
                'global_params': {'metadata': MetaData(pid)},
                'surface_eeg': {},
                'save_and_return': {'output_folder': out_folder}
            })

            data = Data(pid, get_path(), args.task, batch_size,
                        observation_time, margin_time)

            for counter, (X, Y) in enumerate(data):
                workflow.run(inputs=
                    {'X': X, 'Y': Y,
                     'output_filename': '{}_{}.hdf5'.format(pid, counter)}
                )

            logger.info(data.get_raport())

            with open(has_finished_filename, 'w') as f:
                f.write('{}_success'.format(pid))

        return {'in_folder': out_folder}
import signal
from multiprocessing import Pool

import numpy as np
import pandas as pd
from sklearn.base import TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import FunctionTransformer
from sklearn.decomposition import FastICA
from scipy import stats
from scipy.signal import hilbert

from daftlearning.logger import logger
from ieeg.settings import parse_settings


#### Preprocessing Transformers ####

class Choose10_20Canals(TransformerMixin):
    surface_elecs = {
        'FP1', 'FP2', 'F7', 'F3', 'FZ', 'F4',
        'F8', 'T3', 'C3', 'CZ', 'C4',
        'T4', 'T5', 'P3', 'PZ', 'P4',
        'T6', 'O1', 'O2'
    }

    def __init__(self, channels):
        self.channels = channels

    def fit(self, X, y=None):
        return self

    def fit_transform(self, X, y=None, **fit_params):
        is_surface = map(lambda x: x in self.surface_elecs,
                         self.channels)
        indices = np.nonzero(is_surface)[0]

        return X[indices, :]

class ChooseHighestVariance(TransformerMixin):
    def __init__(self, channels, n=19):
        self.n = n

    def fit(self, X, y=None):
        var = np.var(X, axis=1)
        ids = range(var.shape[0])
        sids = sorted(ids, key=lambda x: var[x], reverse=True)
        self.good_ids = sids[:self.n]

        return self

    def transform(self, X):
        return X[self.good_ids]

def ica_transformer(channels):
    return Pipeline([
        ('Transpose', FunctionTransformer(lambda x: x.T)),
        ('ICA', FastICA(n_components=85)),
        ('Transpose2', FunctionTransformer(lambda x: x.T)),
        ('highestVariance', ChooseHighestVariance(channels))
    ])

#### Feature Transformers ####

def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)

def apply_one((pipeline, X))	:
    return pipeline.fit_transform(X)

class FeatureUnion(TransformerMixin):
    def __init__(self, steps, n_jobs=4):
        def add_name(t):
            if type(t) == tuple and len(t) == 2:
                return t
            return (str(type(t)), t)

        self.steps = map(add_name, steps)
        self.n_jobs = n_jobs
        args = parse_settings()
        self.multiprocessing = args.multiprocessing

    def fit(self, X, y=None):
        return self

    def fit_transform(self, X, Y=None):
        if self.multiprocessing:
            P = Pool(self.n_jobs, init_worker)
            try:
                results = P.map(apply_one, [(T[1], X) for T in self.steps])
            except KeyboardInterrupt as e:
                logger.error("Caught KeyboardInterrupt, terminating workers")
                P.terminate()
                P.close()
                raise e
            P.close()
        else:
            results = map(apply_one, [(T[1], X) for T in self.steps])

        return np.concatenate(results, axis=2)

class Windower(TransformerMixin):
    def __init__(self, ratio):
        self.ratio = ratio

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        X = np.transpose(X, axes=(0, 2, 1))
        b, n, c = X.shape
        if n % self.ratio:
            logger.warning('X has shape: {}, {} is not divisible of {}. '
                            'Cropping part of data.'.format(X.shape, n,
                                                            self.ratio))

        X = X.reshape((b, self.ratio, n / self.ratio, c))
        return np.transpose(X, axes=(0, 1, 3, 2))

class Debbuger(TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        logger.debug(X.shape)
        return X

def upper_right_triangle(matrix):
    accum = []
    for i in range(matrix.shape[0]):
        for j in range(i+1, matrix.shape[1]):
            accum.append(matrix[i, j])

    return np.array(accum)

class Stats(TransformerMixin):
    def __init__(self):
        self.stats_fn = [np.std, np.min, np.max, np.median,
                         stats.skew, stats.kurtosis]

    def apply_one(self, data, meta=None):
        # data[ch][dim]
        shape = data.shape
        out = np.empty((shape[0], len(self.stats_fn)))
        for i in range(len(data)):
            ch_data = data[i].astype(np.float64)
            ch_data -= np.mean(ch_data)
            for stat_j, stat in enumerate(self.stats_fn):
                out[i][stat_j] = stat(ch_data)

        return out.flatten()

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        b, w, c, n = X.shape
        out = np.empty((b, w, c * len(self.stats_fn)))
        for bb in range(b):
            for ww in range(w):
                out[bb][ww] = self.apply_one(X[bb][ww])

        return out


class Correlation(TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        b, w, c, n = X.shape
        M = c * (c - 1) / 2
        ret = np.zeros((b, w, M + c), dtype=np.float32)
        for i in xrange(b):
            for j in xrange(w):
                mcoef = np.corrcoef(X[i, j, :, :])
                out = upper_right_triangle(mcoef).flatten()
                eigenvalues, _ = np.linalg.eig(mcoef)
                eigenvalues = np.absolute(eigenvalues)
                eigenvalues.sort()

                ret[i, j, :M] = out
                ret[i, j, M:] = eigenvalues

        return ret

class PSDSumPerBin(TransformerMixin):
    def __init__(self, freq_bins, freq_sampling):
        self.freq_bins = map(float, freq_bins)
        self.freq_sampling = float(freq_sampling)

    def fit(self, X, y=None):
        self.signal_len = (X.shape[-1] - 1) * 2.
        return self

    def transform(self, X):
        b, w, c, n = X.shape
        # count PSD
        def norm(data):
            s = np.reshape(np.sum(data, axis=3) + 1e-12, (b, w, c, 1))
            return data / s

        X = X ** 2
        X = norm(X)

        # count bins
        bin_shape = X.shape[:-1] + (len(self.freq_bins) - 1,)
        bins = np.zeros(bin_shape)
        prev = self.freq_bins[0]
        for i, cur in enumerate(self.freq_bins[1:]):
            idx1 = int(np.floor((prev / self.freq_sampling) * self.signal_len))
            idx2 = int(np.floor((cur / self.freq_sampling) * self.signal_len))
            bin = X[:, :, :, idx1:idx2]
            bins[:, :, :, i] = np.sum(bin, axis=3)
            prev = cur

        return bins

class ShannonEntropy(TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        b, w, c, n = X.shape
        X = X * np.log2(X + 1e-12)
        X = -np.sum(X, axis=3) / np.log2(n)
        return X

class Log10(TransformerMixin):
    # TODO - Tests
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        indx = np.where(X <= 0)
        X[indx] = np.max(X)
        X[indx] = np.min(X) * 0.1
        return X

class FlattenChannels(TransformerMixin):
    # TODO - Tests
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        b, w, c, n = X.shape
        return X.reshape((b, w, c * n))

class PhaseSynchrony(TransformerMixin):
    """
        Calculate phase synchrony between channels using Hilbert transform and Shannon entropy.

        Method described in:
        http://www.researchgate.net/publication/222567264_Comparison_of_Hilbert_transform_and_wavelet_methods_for_the_analysis_of_neuronal_synchrony/links/0deec52baa808a3812000000
        Le Van Quyen M, Foucher J, Lachaux J-P, Rodriguez E, Lutz A, Martinerie JM, Varela FJ (2001)
            Comparison of Hilbert transform and wavelet methods for the analysis of neural synchrony.
            J Neurosci Methods 111:83-98

        Very slow! ~2min per observation
    """
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        batch, window, c, n = X.shape
        h = X + (1j * hilbert(X))
        phase = np.angle(h)

        num_bins = int(np.exp(0.626 + 0.4 * np.log(n - 1)))
        Hmax = np.log(num_bins)

        out = np.empty((batch, window,  c * (c - 1) / 2), dtype=np.float64)
        idx = 0
        for i in range(c):
            for j in range(i + 1, c):
                ch1_phase = phase[:, :, i, :]
                ch2_phase = phase[:, :, j, :]

                phase_diff = np.mod(np.abs(ch1_phase - ch2_phase), np.pi * 2.0)

                hist = np.apply_along_axis(
                    lambda x: np.histogram(x, bins=num_bins)[0],
                    axis=-1, arr=phase_diff
                )
                ssums = np.sum(hist, axis=-1).reshape((batch, window, 1))
                pdf = hist.astype(np.float64) / ssums

                H = np.sum(pdf * np.log(pdf + 1e-12), axis=-1)
                p = (H + Hmax) / Hmax

                out[:, :, idx] = p
                idx += 1

        return out

class HFD(TransformerMixin):
    """
    Higuchi fractal dimension per-channel

    Implementation derived from reading:
    http://arxiv.org/pdf/0804.3361.pdf
    F.S. Bao, D.Y.Lie,Y.Zhang,"A new approach to automated epileptic diagnosis using EEG
    and probabilistic neural network",ICTAI'08, pp. 482-486, 2008.
    """
    # TODO test like PhaseSynchrony
    def __init__(self, kmax):
        self.kmax = kmax

    def _hdf(self, channel):
        """
        :param channel: 1-D slice
        :return: float
        """
        N = len(channel)
        Nm1 = float(N - 1)
        L = np.empty((self.kmax,))
        L[0] = np.sum(abs(np.diff(channel, n=1)))
        for k in xrange(2, self.kmax + 1):
            Lmks = np.empty((k,))
            for m in xrange(1, k + 1):
                i_end = (N -m ) / k # int
                Lmk_sum = np.sum(abs(np.diff(channel[np.arange(m - 1, m + (i_end + 1) * k - 1, k)], n=1)))
                Lmk = Lmk_sum * Nm1 / (i_end * k)
                Lmks[m - 1] = Lmk

            L[k - 1] = np.mean(Lmks)

        a = np.empty((self.kmax, 2))
        a[:, 0] = np.log(1.0 / np.arange(1.0, self.kmax + 1.0))
        a[:, 1] = 1.0

        b = np.log(L)

        # find x by solving for ax = b
        x, residues, rank, s = np.linalg.lstsq(a, b)
        return x[0]

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return np.apply_along_axis(self._hdf, axis=-1, arr=X)

class PFD(TransformerMixin):
    """
    Petrosian fractal dimension per-channel

    Implementation derived from reading:
    http://arxiv.org/pdf/0804.3361.pdf
    F.S. Bao, D.Y.Lie,Y.Zhang,"A new approach to automated epileptic diagnosis using EEG
    and probabilistic neural network",ICTAI'08, pp. 482-486, 2008.
    """
    # TODO test like PhaseSynchrony
    def _pfd(self, ch):
        """
        :param ch: 1-D slice
        :return: float
        """
        diff = np.diff(ch, n=1, axis=0)

        asign = np.sign(diff)
        sign_changes = ((np.roll(asign, 1) - asign) != 0).astype(int)
        N_delta = np.count_nonzero(sign_changes)

        n = len(ch)
        log10n = np.log10(n)
        return log10n / (log10n + np.log10(n / (n + 0.4 * N_delta)))

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return np.apply_along_axis(self._pfd, axis=-1, arr=X)

class Hurst(TransformerMixin):
    """
    Hurst exponent per-channel, see http://en.wikipedia.org/wiki/Hurst_exponent

    Another description can be found here: http://www.ijetch.org/papers/698-W10024.pdf
    Kavya Devarajan, S. Bagyaraj, Vinitha Balasampath, Jyostna. E. and Jayasri. K.,
    "EEG-Based Epilepsy Detection and Prediction," International Journal of Engineering
    and Technology vol. 6, no. 3, pp. 212-216, 2014.

    """
    # TODO test like PhaseSynchrony
    def _hurst(self, x):
        """
        :param x: 1-D slice
        :return: float
        """
        x = x.astype(np.float64)
        x -= x.mean()
        z = np.cumsum(x)
        r = (np.maximum.accumulate(z) - np.minimum.accumulate(z))[1:]
        s = pd.expanding_std(x)[1:]

        # prevent division by 0
        s[np.where(s == 0)] = 1e-12
        r += 1e-12

        y_axis = np.log(r / s)
        x_axis = np.log(np.arange(1, len(y_axis) + 1))
        x_axis = np.vstack([x_axis, np.ones(len(x_axis))]).T

        m, b = np.linalg.lstsq(x_axis, y_axis)[0]
        return m

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        return np.apply_along_axis(self._hurst, axis=-1, arr=X)


if __name__ == "__main__":
    from seizure_prediction.transforms import Stats as H
    T1 = H()
    T2 = Stats()

    from ieeg.parseHeader import read_data
    h, data = read_data("/Volumes/LESZCZU/eeg/pat_11502/adm_115102/rec_11500102/11500102_0000.data")

    data.resize((1,) + data.shape)
    W = Windower(8)
    data = W.fit_transform(data)

    from datetime import datetime
    s1 = datetime.now(); out2 = T2.fit_transform(data); s2 = datetime.now(); print s2 - s1
    s1 = datetime.now(); out1 = T1.apply(data.reshape(data.shape[1:]), {}); s2 = datetime.now(); print s2 - s1

    print np.array_equal(out1, out2.reshape(out2.shape[1:]))
    from ipdb import set_trace; set_trace()

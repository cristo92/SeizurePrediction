#!/usr/bin/python
#-*- coding: utf-8 -*-
#© Copyright 2013 Piotr Milanowski. All Rights Reserved.
# try:
#     import psycopg2
#     true = 'true'
#     false = 'false'
#     def sql_get(sql, db="dbname=epilepsiae user=milan"):
#         conn = psycopg2.connect(db)
#         cursor = conn.cursor()
#         cursor.execute(sql)
#         return cursor
# except (ImportError, prsycopg2.OperationalError):
import numpy as np
import sqlite3
import json
from operator import itemgetter
true = 1
false = 0

DATABASE = 'epi_with_headers.db'

def sql_get(sql):
    conn = sqlite3.connect(DATABASE)
    c = conn.cursor()
    c.execute(sql)
    return c

def get_patients():
    """
    Returns all patient IDs form the DB
    """ 
    SQL = "SELECT * FROM patient;"
    return sql_get(SQL)
def code2ID(code):
    """
    Return patients id based on patients code
    """
    SQL = "SELECT id FROM patient WHERE patientcode='%s';"%code
    cursor = sql_get(SQL)
    i = next(cursor)
    return i[0]

def get_seizures_by_patient(patient_id, all_=False):
    """
    Returns files containing seizures for a given patient

    Rows returned are:
    path, name, eeg_onset, eeg_offset, seizure ID

    If all_ is true, all files with recorded seizures are returned; else only
    ones without spikes and subclinical events
    """
    #Blocks with out spikes and subclinical events
    SQL_admission = "SELECT admission.id FROM admission WHERE admission.patient=%s"%patient_id
    SQL_all_blocks = """
    SELECT bl.id
    FROM block as bl, files as fl, ({sql_admission}) AS adm
    WHERE adm.id=fl.admission AND bl.eeg_file=fl.id""".format(sql_admission=SQL_admission)
    SQL_seizure_blocks = """
    SELECT DISTINCT a.id
    FROM ({sql_all_blocks}) AS a, seizure as s
    WHERE s.block=a.id
    """.format(sql_all_blocks=SQL_all_blocks)
    SQL_spike_blocks = """
    SELECT DISTINCT a.id
    FROM ({sql_all_blocks}) AS a, spike AS s
    WHERE s.block=a.id
    """.format(sql_all_blocks=SQL_all_blocks)
    SQL_subclinical_blocks = """
    SELECT DISTINCT a.id
    FROM ({sql_all_blocks}) AS a, subclinicalevent AS s
    WHERE s.block=a.id
    """.format(sql_all_blocks=SQL_all_blocks)
    SQL_clean_seizure_blocks = """
    {sql_all}
    EXCEPT
    {sql_spike}
    EXCEPT
    {sql_sub}
    """.format(sql_all=SQL_all_blocks,
            sql_spike=SQL_spike_blocks,
            sql_sub=SQL_subclinical_blocks)
    if all_:
        SQL_clean_seizure_blocks = SQL_seizure_blocks
    SQL_ = """
    SELECT fl.path,
            fl.name,
            fl.id,
            sz.eeg_onset,
            sz.eeg_offset,
            sz.clin_onset,
            sz.clin_offset,
            sz.pattern,
            sz.classification,
            sz.vigilance,
            sz.id,
            bl.recording
    FROM files AS fl,
        ({sql_clean}) AS clean,
        seizure AS sz,
        block AS bl
    WHERE sz.block=clean.id AND bl.id=clean.id AND bl.eeg_file=fl.id;
    """.format(sql_clean=SQL_clean_seizure_blocks)
    header = (
        'file_path', 'file_name', 'file_id',
        'seizure_eeg_onset', 'seizure_eeg_offset',
        'seizure_clin_onset', 'seizure_clin_offset',
        'seizure_pattern', 'seizure_classification',
        'seizure_vigilance', 'seizure_id',
        'block_recording'
    )
    ret = list(sql_get(SQL_))
    return map(lambda x: dict(zip(header, x)), ret)

def get_electrodes(recording):
    """
    Return electrodes and their position used in given recording
    """
    SQL_electrodes_recording = """
                SELECT electrode, position FROM electrode_usage WHERE recording={rec}
                """.format(rec=recording)
    SQL = """
    SELECT e.name, e.coord_x, e.coord_y, e.coord_z, eu.position
    FROM electrode AS e JOIN ({sql_electrodes}) as eu ON eu.electrode=e.id ORDER BY eu.position
    """.format(sql_electrodes=SQL_electrodes_recording)
    ret = list(sql_get(SQL))
    return map(lambda x: {'electrode_name': x[0], 
                          'coordinates': np.array(x[1:4]), 
                          'pos': x[4]}, ret)

def get_notseizures_by_patient(patient_id):
    """
    Returns files without seizures for a given patient
    """
    SQL_admission = "SELECT admission.id FROM admission WHERE admission.patient=%s"%patient_id
    SQL_all_files = """
            SELECT bl.id, fl.path, fl.name
            FROM files AS fl, ({sql_admission}) AS adm, block AS bl
            WHERE adm.id=fl.admission AND bl.eeg_file=fl.id
            """.format(sql_admission=SQL_admission)
    SQL_seizure_files = """
            SELECT DISTINCT a.id, a.path, a.name
            FROM ({sql_all_files}) AS a, seizure AS s
            WHERE s.block=a.id
            """.format(sql_all_files=SQL_all_files)
    SQL_spike_files = """
            SELECT DISTINCT a.id, a.path, a.name
            FROM ({sql_all_files}) AS a, spike AS s
            WHERE s.block=a.id
            """.format(sql_all_files=SQL_all_files)
    SQL_subclinical_files = """
            SELECT DISTINCT a.id, a.path, a.name
            FROM ({sql_all_files}) AS a, subclinicalevent AS s
            WHERE s.block=a.id
            """.format(sql_all_files=SQL_all_files)
    SQL_not_seizure_blocks = """
    {sql_all_files}
    EXCEPT
    {sql_seizure_files}
    EXCEPT
    {sql_spike_files}
    EXCEPT
    {sql_subclinical_files}
    """.format(sql_all_files=SQL_all_files, 
            sql_seizure_files=SQL_seizure_files,
            sql_spike_files=SQL_spike_files,
            sql_subclinical_files=SQL_subclinical_files)
    SQL = """
        SELECT fl.path,
               fl.name,
               fl.id,
               bl.recording
        FROM files AS fl,
            ({sql_clean}) AS clean,
            block AS bl
        WHERE bl.id=clean.id AND bl.eeg_file=fl.id; 
    """.format(sql_clean=SQL_not_seizure_blocks)
    header = (
        'file_path', 'file_name', 'file_id', 'block_recording'
    )
    ret = list(sql_get(SQL))
    ret = map(lambda x: dict(zip(header, x)), ret)
    return ret

def get_invasive_electrodes(recording):
    """
    Return list of names of invasive electrodes used in the measurement
    """
    SQL = """
    SELECT e.name
    FROM electrode as e,
        (SELECT e.electrode
        FROM electrode_usage as e
        WHERE e.recording = %s) as electrodes
    WHERE electrodes.electrode = e.id AND e.invasive=%s;
    """%(recording, str(true))
    return sql_get(SQL)
def get_propagation(seizure, mode="origin"):
    """
    Return propagation electrodes;
    mode can be: origin, early_propagation or late_propagation
    for a given seizure
    """
    if mode == "origin":
        what = "'%Electrode:%'"
    elif mode == "early_propagation":
        what = "'%Prop 1:%'"
    elif mode == "late_propagation":
        what = "'%Prop 2:%'"
    else:
        raise ValueError
    SQL = """
    SELECT el.name FROM electrode AS el,
        (SELECT electrode FROM propagation
            WHERE propagation.seizure=%s
            AND commentary like %s) AS props
    WHERE props.electrode=el.id;
    """%(seizure, what)
    return map(itemgetter(0), list(sql_get(SQL)))

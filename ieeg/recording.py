#!/usr/bin/python
#-*- coding: utf-8 -*-
from parseHeader import read_head, read_data
import dbQueries
import datetime

class seizureStruct(object):
    """A container for information about one seizure
    """
    def __init__(self, query_tuple, init_path="/run/media/milan/Seagate Backup Plus Drive/epilepsy_data/"):
        """
        Query tuple is returned by dbQeueries.get_seizure_by_patient
        """
        self.id_ = query_tuple[-2]#File ID
        self.rec_id = query_tuple[-1]
        self.pattern = query_tuple[6]
        self.classification = query_tuple[7]
        self.vigilance = query_tuple[8]
        self.file_ = query_tuple[0][5:]+query_tuple[1][:-5]
        self.onset, self.offset, self.onset_ts, self.offset_ts, fs = self.__get_onset_and_offset(query_tuple, init_path=init_path)
        self.origin = self.__get_propagation()
        self.fs = int(fs)
        self.late_propagation = self.__get_propagation("late_propagation")
        self.early_propagation = self.__get_propagation("early_propagation")
        self.invasive_electrodes = [i[0] for i in list(dbQueries.get_invasive_electrodes(str(self.rec_id)))]
    def __get_onset_and_offset(self, query_tuple, sec=False, init_path="/run/media/milan/Seagate Backup Plus Drive/epilepsy_data/"):
        """
        Returns onset and offset of seizure with regards to beginning of the file
        and seizure onset and offset timestamp
        In points if sec is False, else in seconds.
        """
        fmt = "%Y-%m-%d %H:%M:%S"
        head = read_head(init_path+self.file_)
        fs = head['sample_freq']
        on = query_tuple[4] if query_tuple[2] is None else query_tuple[2]#Timestamp
        off = query_tuple[5] if query_tuple[3] is None else query_tuple[3]#Timestamp
	try:
	    on = datetime.datetime.strptime(on, fmt)
	except ValueError:
	    on = datetime.datetime.strptime(on, fmt+".%f")
	try:
	    off = datetime.datetime.strptime(off, fmt)
	except ValueError:
	    off = datetime.datetime.strptime(off, fmt+".%f")
        on_ = on - head['start_ts']
        off_ = off - head['start_ts']
        on_ = on_.total_seconds()
        off_ = off_.total_seconds()
        if sec:
            return on_, off_, on, off, fs
        else:
            return int(on_*fs), int(off_*fs), on, off, fs
    def __get_propagation(self, mode="origin"):
        c = dbQueries.get_propagation(str(self.id_), mode)
        return [el[0] for el in c]

def recalculate_data(list_of_seizures, init_path=None):
    """Checks if there are several seizures in one file.

    Returns file with corresponding seizure lists
    """
    structure = {}
    for seizure in list_of_seizures:
        if init_path is None:
            seizure_data = seizureStruct(seizure)
        else:
            seizure_data = seizureStruct(seizure, init_path=init_path)
        if structure.has_key(seizure_data.file_):
            structure[seizure_data.file_].append(seizure_data)
        else:
            structure[seizure_data.file_] = [seizure_data]
    return structure

class recording(object):
    def __init__(self, file_, list_of_seizures, init_path="/run/media/milan/Seagate Backup Plus Drive/epilepsy_data/"):
        """
        file_ is the record file
        list_of_seizures is a list of seizureStructs
        """
        self.file_ = file_
        self.seizure_list = list_of_seizures
        self.invasive_electrodes = list_of_seizures[0].invasive_electrodes
        print "Reading file %s... This may take a while"%file_
        self.header, self.data = read_data(init_path+self.file_)
        print "Done"
    def get_indices(self, seizure_no=0, mode="origin"):
        seizure = self.seizure_list[seizure_no]
        indices = []#self.header['elec_names'].index(el) for el in {"origin": seizure.origin,
            #"late_propagation":seizure.late_propagation, "early_propagation":seizure.early_propagation}[mode]]
        where_to_look = {"all":self.invasive_electrodes, "origin": seizure.origin, "late_propagation":seizure.late_propagation, "early_propagation":seizure.early_propagation}[mode]
        for el in where_to_look:
            try:
                indices.append(self.header['elec_names'].index(el))
            except ValueError:
                el_d = {"FT10":"T2", "FT9":"T1", "T8":"T4", "T7":"T3", "P8":"T6", "P7":"T5"}
                if el_d.has_key(el):
                    el_ = el_d[el]
                else:
                    raise ValueError("To juz nie wiem...")
                indices.append(self.header['elec_names'].index(el_))
        if not indices:
            return []#self.get_indices_by_name(None)
        return indices
    def get_indices_by_name(self, indices=None):
        if indices is None:
            return [self.header['elec_names'].index(el) for el in self.invasive_electrodes]
        else:
            return [self.header['elec_names'].index(el) for el in indices]
    def montage(self):
        """
        Calculates montage
        """
        idx = self.get_indices_by_name()
        mean = self.data[idx,:].mean(0)
        self.data = self.data - mean[None, :]


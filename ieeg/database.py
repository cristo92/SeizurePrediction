import sqlite3
import json
import ast
from datetime import datetime, timedelta
from operator import itemgetter

from dateutil.parser import parse

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Database(object):
    __metaclass__ = Singleton

    def __init__(self, DATABASE='epi_with_headers.db'):
        self.conn = sqlite3.connect(DATABASE)
        self.block_headers = ["id", "recording", "eeg_file", "block_no", "samples",
                   "sample_bytes", "channels", "factor", "begin", "end", "gap"]
        self.header_headers = ["file_id", "adm_id", "pat_id", "elec_names", "sample_freq",
                   "start_ts", "duration_in_sec", "rec_id", "conversion_factor",
                   "num_channels", "num_samples", "sample_bytes"]

    @property
    def cursor(self):
        return self.conn.cursor()

    def commit(self):
        return self.conn.commit()

    def sql_insert(self, sql, *args):
        res = self.cursor.execute(sql, args)
        self.conn.commit()
        return res

    def sql_get(self, sql):
        return self.cursor.execute(sql)

    def get_recordings(self, pid):
        query = """
        SELECT * FROM recording WHERE admission
        IN (
            SELECT id FROM admission WHERE patient = {}
        )
        """.format(pid)

        headers = ["id", "str_id", "admission", "begin", "end",
                   "duration", "blocks", "channels", "sample_rate",
                   "backgRhythm", "EEGslowing", "iEEGslowing", "commentary",]

        return map(lambda x: dict(zip(headers, x)), self.sql_get(query))

    def _parse_block(self, block):
        block = dict(zip(self.block_headers, block))
        block['id'] = int(block['id'])
        block['gap'] = block['gap'] or 0
        format = "%Y-%m-%d %H:%M:%S"
        block['begin'] = parse(block["begin"])
        block['end'] = parse(block["end"])

        return block

    def get_blocks(self, recording_id, block_nr=None):
        """

        :param recording_id:
        :param block_nr:  block number from which you'd like to start investigation
        :return:
        """
        if block_nr is None:
            query = """
            SELECT * FROM block WHERE recording = {} ORDER BY begin
            """.format(recording_id)
        else:
            query = """
            SELECT * FROM block WHERE recording = {} AND id >= {} ORDER BY begin
            """.format(recording_id, block_nr)

        return map(self._parse_block, self.sql_get(query))

    def _parse_header(self, header):
        header = dict(zip(self.header_headers, header))
        header['elec_names'] = ast.literal_eval(header['elec_names'][1:-1]) # Removing trailing and leading apostrophe
        return header

    def get_header(self, file_id):
        query = """
        SELECT * FROM headers WHERE file_id = {}
        """.format(file_id)

        try:
            return map(self._parse_header, self.sql_get(query))[0]
        except IndexError as e:
            print file_id
            raise e

    def get_all_blocks(self):
        query = """
        SELECT * FROM block
        """

        return map(self._parse_block, self.sql_get(query))

    def get_file(self, file_id):
        query = """
        SELECT * FROM files WHERE id = {}
        """.format(file_id)

        headers = ["id", "admission", "name", "path", "format", "length", "checksum"]

        return map(lambda x: dict(zip(headers, x)), self.sql_get(query))[0]

    def add_header(self, file_id, elec_names, adm_id, pat_id, sample_freq, start_ts, 
                   duration_in_sec, rec_id, conversion_factor, num_channels,
                   num_samples, sample_bytes):
        start_ts = "'" + start_ts.strftime("%Y-%m-%d %H:%M:%S.%f") + "'"
        elec_names = "'" + json.dumps(elec_names) + "'"
        SQL = """
        INSERT INTO headers (file_id, elec_names, adm_id, pat_id, sample_freq, start_ts, 
                   duration_in_sec, rec_id, conversion_factor, num_channels,
                   num_samples, sample_bytes)
        VALUES (
        """ + ", ".join(["?"] * 12) + ")"

        return self.sql_insert(SQL, file_id, elec_names, adm_id, pat_id, 
                   sample_freq, start_ts, 
                   duration_in_sec, rec_id, conversion_factor, num_channels,
                   num_samples, sample_bytes)

    def get_headers(self):
        return self.cursor.execute("SELECT * FROM headers")

    def create_headers_table(self):
        """
        elec_names <type 'list'>
        adm_id <type 'str'>
        pat_id <type 'str'>
        sample_freq <type 'int'>
        start_ts <type 'datetime.datetime'>
        duration_in_sec <type 'str'>
        rec_id <type 'str'>
        conversion_factor <type 'float'>
        num_channels <type 'str'>
        num_samples <type 'int'>
        sample_bytes <type 'int'>
        """

        return self.sql_insert("""
        CREATE TABLE main.headers(
            file_id BIGINT PRIMARY KEY,
            adm_id INT,
            pat_id INT NOT NULL,
            elec_names TEXT,
            sample_freq INT,
            start_ts VARCHAR(255),
            duration_in_sec INT,
            rec_id INT,
            conversion_factor FLOAT,
            num_channels INT,
            num_samples INT,
            sample_bytes INT
        )
        """)

    SEIZURE = 1
    NON_SEIZURE = 0
    UNKNOWN = -1
    SUBCLINICAL = 1

    def scan(self, pid, start, end):
        """
        :param pid: int
        :param start: datetime
        :param end: datetime
        :return: -1 missing data
                  0 no_seizure
                  1 seizure
        """

        # Look for seizure inside time interval
        q_recording_with_subclinical = """
        SELECT patient FROM admission WHERE id in (
            SELECT admission FROM recording WHERE id IN (
                SELECT recording FROM seizure WHERE
                clin_offset > "{beg}" AND clin_onset < "{end}"
                UNION
                SELECT recording FROM subclinicalevent WHERE
                offset > "{beg}" AND onset < "{end}"
            )
        )
        """.format(beg=str(start), end=str(end))
        q_recording = """
        SELECT patient FROM admission WHERE id in (
            SELECT admission FROM recording WHERE id IN (
                SELECT recording FROM seizure WHERE
                clin_offset > "{beg}" AND clin_onset < "{end}"
            )
        )
        """.format(beg=str(start), end=str(end))

        pids = list(self.sql_get(q_recording))

        if pid in map(itemgetter(0), pids):
            return self.SEIZURE

        # Check if there is too big gap inside time interval
        # Adnotation: this is so ugly, that it's begging to be refactored
        q_recordings = """
        SELECT id FROM recording
        WHERE begin < "{beg}" AND end > "{end}"
        AND admission IN
        (SELECT id FROM admission WHERE patient={pid})
        """.format(beg=str(start), end=str(end), pid=pid)

        recordings = list(self.sql_get(q_recordings))
        if len(recordings) == 0:
            return self.UNKNOWN

        recording_id = recordings[0]
        q_blocks_with_gaps = """
        SELECT begin, end, gap FROM block WHERE recording={recording_id} AND end > "{beg}" ORDER BY begin
        """.format(recording_id=recording_id[0], beg=str(start))

        blocks_with_gaps = list(self.sql_get(q_blocks_with_gaps))
        headers = ["begin", "end", "gap"]
        blocks = map(lambda x: dict(zip(headers, x)), blocks_with_gaps)
        blocks.reverse()

        finish = len(blocks)
        while finish and len(blocks):
            block = blocks.pop()

            if parse(block['begin']) > start and block['gap'] > 60:
                return self.UNKNOWN
            if parse(block['end']) > end:
                finish = 0
            if end - parse(block['end']) < timedelta(0, 60):
                break

        return self.NON_SEIZURE


if __name__ == "__main__":
    DB = Database()
    print DB.scan(58702, parse("2008-12-01 20:19:28.983398"), parse("2008-12-01 20:19:30.983398")) #0
    print DB.scan(58702, parse("2008-12-01 20:21:24"), parse("2008-12-01 20:23:24")) #0
    print DB.scan(107302, parse("2008-12-01 20:19:28.983398"), parse("2008-12-01 20:19:30.983398")) #1
    print DB.scan(107302, parse("2008-12-01 20:21:24"), parse("2008-12-01 20:23:24")) #1
    print DB.scan(107302, parse("2008-12-01 20:19:30.983398"), parse("2008-12-01 20:21:24")) #1
    print DB.scan(107302, parse("2008-12-01 20:19:27.983398"), parse("2008-12-01 20:19:28.983398"))  # 0

    if False:
        DB = Database()
        from ieeg.data_info import get_patient_pids
        from operator import itemgetter

        recordings = reduce(lambda x, y: x + DB.get_recordings(y), get_patient_pids(), [])
        blocks = reduce(lambda x, y: x + DB.get_blocks(y['id']), recordings, [])
        recordings = dict(map(lambda x: (x['id'], x), recordings))

        for block in blocks:
            freq = recordings[block['recording']]['sample_rate']
            assert(block["samples"] == (block["end"] - block["begin"]).total_seconds() * freq)

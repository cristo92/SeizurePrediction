import numpy as np

from daftlearning.workflow.workflow_tasks import WorkflowTask

class SurfaceEEG(WorkflowTask):
	surface_elecs = {
		'FP1', 'FP2', 'F7', 'F3', 'FZ', 'F4',
		'F8', 'T3', 'C3', 'CZ', 'C4',
		'T4', 'T5', 'P3', 'PZ', 'P4',
		'T6', 'O1', 'O2'
	}

	def _run(self, X, Y, metadata, **kwargs):
		is_surface = map(lambda x: x in self.surface_elecs,
			             metadata.electrodes_name)
		indices = np.nonzero(is_surface)[0]

		return {
			'X': X[:, indices, :],
			'Y': Y,
		}
import os

import numpy as np
import pandas as pd
from daftlearning.logger import logger
from daftlearning.workflow.workflow_tasks import WorkflowTask
from sklearn.metrics import roc_auc_score


class CrossValidation(WorkflowTask):
    def _run(self, X_test, Y_test, args, model, metadata_path, **kwargs):
        b, w, n = X_test.shape
        preds = np.zeros((b, w))

        for i in xrange(w):
            preds[:, i] = model.predict(X_test[:, i, :])
        preds = np.sum(preds, axis=1) / w
        #preds = preds.flatten()
        #Y_test = np.array(zip(*[Y_test] * w)).flatten()

        logger.info("ROC_AUC: {}".format(roc_auc_score(Y_test, preds)))

        data = pd.DataFrame({'preds': preds, 'labels': Y_test})
        data.to_csv(os.path.join(metadata_path, 'preds.csv'))

        return {'preds_test': preds, 'Y_test': Y_test}
from daftlearning.workflow.workflow_tasks import WorkflowTask
from ieeg.database import Database

class SaveResultDatabase(WorkflowTask):
    def _run(self, preprocessing, classifier, metadata_path, pids, roc_auc, prec_rec_auc, log_loss, **kwargs):
        DB = Database()
        query = """
        INSERT OR REPLACE INTO results (preprocessing, classifier, logpath, pid, roc_auc, prec_rec_auc, log_loss) 
        VALUES (?, ?, ?, ?, ?, ?, ?);
        """
        DB.sql_insert(query, *[preprocessing, classifier, metadata_path, str(sorted(pids)), roc_auc, prec_rec_auc, log_loss])

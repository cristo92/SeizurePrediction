import os

import numpy as np
import pandas as pd
from sklearn.metrics import precision_recall_curve, auc, roc_auc_score, log_loss

from daftlearning.workflow.workflow_tasks import WorkflowTask
from daftlearning.logger import logger

class AddResultsToCSV(WorkflowTask):
    def _pr_auc(self, y, p):
        precision, recall, _ = precision_recall_curve(y, p)
        return auc(recall, precision)

    def _run(self, args, label, train_pids, valid_pids, test_pids, Y_train, Y_valid, Y_test, preds_test,
             preds_valid, preds_train, metadata_path, **kwargs):
        metrics = {
            'ROC_AUC': roc_auc_score,
            'PREC_REC_AUC': self._pr_auc,
            'LOG_LOSS': log_loss
        }

        row = {
            "label": [label],
            "pids_train": [train_pids],
            "pids_valid": [valid_pids],
            "pids_test": [test_pids],

            "train_roc_auc": [roc_auc_score(Y_train, preds_train)],
            "train_prec_rec_auc": [self._pr_auc(Y_train, preds_train)],
            "train_log_loss": [log_loss(Y_train, preds_train)],

            "valid_roc_auc": [roc_auc_score(Y_valid, preds_valid)],
            "valid_prec_rec_auc": [self._pr_auc(Y_valid, preds_valid)],
            "valid_log_loss": [log_loss(Y_valid, preds_valid)],

            "test_roc_auc": [roc_auc_score(Y_test, preds_test)],
            "test_prec_rec_auc": [self._pr_auc(Y_test, preds_test)],
            "test_log_loss": [log_loss(Y_test, preds_test)],

            "Ys": ["{}/{}/{}".format(np.sum(Y_train), np.sum(Y_valid), np.sum(Y_test))],
            "Xs": ["{}/{}/{}".format(Y_train.shape[0], Y_valid.shape[0], Y_test.shape[0])]
        }

        data = pd.DataFrame(row, columns=["label", "train_roc_auc", "train_prec_rec_auc", "train_log_loss",
                                          "valid_roc_auc", "valid_prec_rec_auc", "valid_log_loss",
                                          "test_roc_auc", "test_prec_rec_auc", "test_log_loss", "Ys",
                                          "Xs", "pids_train", "pids_valid", "pids_test"])
        logger.info("Appending row {}".format(data))

        filepath = os.path.join(metadata_path, "results.csv")
        if os.path.exists(filepath):
            with open(filepath, 'a') as f:
                data.to_csv(f, header=False)
        else:
            with open(filepath, 'w') as f:
                data.to_csv(f, header=True)
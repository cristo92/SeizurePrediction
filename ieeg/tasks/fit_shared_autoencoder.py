import os

import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from keras.initializers import RandomNormal
from keras.layers import Input, Dense
from keras import regularizers
from keras.models import Model
from keras.optimizers import adam

from daftlearning.workflow.workflow_tasks import WorkflowTask
from daftlearning.logger import logger
from ieeg.database import Database
from ieeg.block import Block
from ieeg.parseHeader import DataShapeException


class FitSharedAutoencoder(WorkflowTask):
    class Autoencoder(object):
        def __init__(self, n_channels, unify_layer, dae_noise, loadname=None, loadpath=None):
            """
            :param n_channels: number of channels in the recording
            :param unify_layer: keras layer, which is shared among every autoencoders
            :param dae_noise: parameter of DenoisingAutoEncoder - probability that the value in X will be replaced
            :param loadname: str: filename, from which you can read weights
            :param loadpath: str: path to the filename above
            """
            inputs = Input(shape=(n_channels,), name="input")
            l1 = Dense(40, kernel_regularizer=regularizers.l2(1e-3), activation="relu",
                       kernel_initializer=RandomNormal(mean=0.0, stddev=0.5 / np.sqrt(40 * n_channels)))(inputs)
            l2 = unify_layer(l1)
            l3 = Dense(40, kernel_regularizer=regularizers.l2(1e-3), activation="relu",
                       kernel_initializer=RandomNormal(mean=0.0, stddev=0.5 / np.sqrt(40 * 19)))(l2)
            training_layer = Dense(n_channels,
                                   kernel_initializer=RandomNormal(mean=0.0, stddev=0.5 / np.sqrt(40 * n_channels)))(l3)
            self.unify_layer = Model(inputs=inputs, outputs=l2)
            self.training_layer = Model(inputs=inputs, outputs=training_layer)
            optimizer = adam(lr=0.0001)
            self.training_layer.compile(optimizer=optimizer, loss="mse")
            self.dae_noise = dae_noise

            if loadname is not None:
                self.load(loadname, loadpath)

        def fit(self, X, y):
            self.training_layer.fit(X, y, epochs=1, batch_size=128)

        def unify(self, X):
            return self.unify_layer.predict(X)

        def predict(self, X):
            return self.training_layer.predict(X)

        def fit_transform(self, X):
            """
            This bad name for this function, because it's not actually fitting the Autencoder. I put this name
            for compability with PreprocessingFeatureExtraction
            :param X:
            :return: unification of X
            """
            st = StandardScaler()
            X = st.fit_transform(X).T

            return self.unify(X).T

        def save(self, filename, path=None):
            path = path or ""
            self.unify_layer.save_weights(os.path.join(path, "unify_" + filename))
            self.training_layer.save_weights(os.path.join(path, "training_" + filename))

        def load(self, filename, path=None):
            path = path or ""
            self.unify_layer.load_weights(os.path.join(path, "unify_" + filename))
            self.training_layer.load_weights(os.path.join(path, "training_" + filename))

    def __init__(self, *args, **kwargs):
        super(FitSharedAutoencoder, self).__init__(*args, **kwargs)
        self.DB = Database()
        self.dae_noise = None
        self.shuffle = False

        # Model parts
        self.autoencoders = {}  # (pid, n_channels) -> model
        self.unify_layer = Dense(19, kernel_regularizer=regularizers.l2(1e-3),
                                 kernel_initializer=RandomNormal(mean=0.0, stddev=0.5 / np.sqrt(40 * 19)),
                                 name="unifying")

    def random_noise(self, X):
        if self.dae_noise < 0.00000001:
            return X
        bn = np.random.binomial(2, self.dae_noise, X.shape)
        sh = X.copy()
        for i in xrange(sh.shape[1]):
            np.random.shuffle(sh[:, i])

        return bn * sh + (1 - bn) * X

    def fit_autoencoder(self, pid, recording, blocks_per_recording):
        """
        Fitting data from one patient into autoencoder
        :param: pid: (int or string - I don't remember) pid of patient
        :param recording: dict with row from sqlite3.recording
        :return:
        """
        n_channels = recording['channels']
        blocks = self.DB.get_blocks(recording['id'])
        # Getting only part of the blocks for training
        if self.shuffle:
            blocks_all = np.random.choice(blocks, min(len(blocks), blocks_per_recording), False)
            np.random.shuffle(blocks_all)
        else:
            blocks_all = blocks[:21]
        split_n = len(blocks_all) - max(1, int(len(blocks_all)*0.3))
        blocks_train = blocks_all[:split_n]
        blocks_valid = blocks_all[split_n:]

        # Getting model from dict
        model = None
        key = (int(pid), n_channels)
        model = self.autoencoders[key]

        # Fitting model
        for block in blocks_train:
            logger.debug("Fitting block {}".format(block['id']))
            header = self.DB.get_header(block["eeg_file"])

            try:
                bblock = Block(block, recording["channels"], recording["sample_rate"])
            except (IOError, DataShapeException):
                return

            X = bblock.remaining_dataset().T
            st = StandardScaler()
            X = st.fit_transform(X)
            Y = self.random_noise(X)

            model.fit(X, Y)

        # Evaluating model
        errs = []
        for block in blocks_valid:
            logger.debug("Validating block {}".format(block['id']))
            try:
                bblock = Block(block, recording["channels"], recording["sample_rate"])
            except (IOError, DataShapeException):
                return

            X = bblock.remaining_dataset().T
            st = StandardScaler()
            X = st.fit_transform(X)
            errs.append((mean_squared_error(X, model.predict(X)), X.shape[0]))

        if len(errs) > 0:
            sq, weights = zip(*errs)
            avr = np.average(sq, weights=weights)
            logger.info("MSE {}, {}: {:0.4f}".format(pid, n_channels, avr))
        else:
            logger.warning(len(blocks_train), len(blocks_valid), pid, recording)

    def _run(self, pids, state_path, epochs, blocks_per_recording, shuffle=True, force=False,
             dae_noise=0.0, **kwargs):
        self.dae_noise = dae_noise
        self.shuffle = shuffle
        self.blocks_per_recording = blocks_per_recording

        # Load model
        model_path = os.path.join(state_path, "shared_autoencoders")
        if not os.path.exists(model_path):
            os.mkdir(model_path)
        if not force:
            filenames = os.listdir(model_path)
            if filenames != []:
                for filename in filenames:
                    layer, pid, n_channels = filename.split('.')[0].split('_')
                    n_channels = int(n_channels)
                    pid = int(pid)
                    logger.info("Loading DAE from {}".format("{}/{}".format(model_path, filename)))
                    if (pid, n_channels) in self.autoencoders:
                        continue
                    self.autoencoders[(pid, n_channels)] = FitSharedAutoencoder.Autoencoder(
                            n_channels, self.unify_layer, dae_noise, loadname="{}_{}.hdf5".format(pid, n_channels),
                            loadpath=model_path)
                return {
                    "channel_chooser": (
                        lambda header:
                        self.autoencoders[(int(header['pat_id']), len(header['elec_names']))]
                    )}

        # Init model architecture
        layers = {}
        for pid in pids:
            recordings = self.DB.get_recordings(pid)
            for recording in recordings:
                key = (int(pid), recording['channels'])
                if key not in self.autoencoders:
                    self.autoencoders[key] = FitSharedAutoencoder.Autoencoder(
                        recording['channels'], self.unify_layer, dae_noise)

        # Fit models
        for epoch in xrange(epochs):
            logger.info("Staring epoch {}/{}".format(epoch, epochs))
            for pid in pids:
                recordings = self.DB.get_recordings(pid)
                # Fitting
                for recording in recordings:
                    self.fit_autoencoder(pid, recording, blocks_per_recording)


        # Saving model
        for (pat_id, n_channels), model in self.autoencoders.items():
            model.save("{}_{}.hdf5".format(pat_id, n_channels), model_path)


        return {
            "channel_chooser": (
                lambda header:
                    self.autoencoders[(int(header['pat_id']), len(header['elec_names']))]
            )}

import os
import random
from collections import defaultdict
from glob import glob
from operator import itemgetter

import numpy as np
from sklearn.model_selection import TimeSeriesSplit

from daftlearning.workflow.workflow_tasks import WorkflowTask
from daftlearning.logger import logger
from ieeg.utils import shuffle_both


class ReadFeaturesAndSplitByPatientWithValid(WorkflowTask):
    def _run(self, args, valid_size, test_size, pids, train_pids=None, valid_pids=None, test_pids=None,
             shuffle=False, rejected_prefixes=None, **kwargs):
        """
        :param args: command-line arguments
        :param test_size: this not very precise
        :param rejected_prefixes: prefixes of the recording, which shouldn't be included in train/valid
        :param kwargs:
        :return:
        """
        rejected_prefixes = [] or rejected_prefixes
        pids = map(str, pids)
        out_dir = args.output
        counter = [defaultdict(int), defaultdict(int)]

        files = glob(os.path.join(args.output, "*.npy"))
        for f in files:
            f = f.split("/")[-1]
            l, major, minor = f.split('_')
            counter[int(l)][major[:4]] += 1

        for key in counter[0].keys():
            if not any(map(lambda x: x[:4] == key[:4], pids)):
                counter[0].pop(key)

        test_size = max(1, int(len(counter[0]) * test_size))
        valid_size = max(1, int(len(counter[0]) * valid_size))
        if shuffle:
            np.random.shuffle(pids)
        pids = map(lambda x: x[:4], pids)

        if test_pids is None:
            test_pids = pids[:test_size]
            valid_pids = pids[test_size:(test_size + valid_size)]
            train_pids = pids[(test_size + valid_size):]
        else:
            train_pids = map(lambda x: str(x)[:4], train_pids)
            valid_pids = map(lambda x: str(x)[:4], valid_pids)
            test_pids = map(lambda x: str(x)[:4], test_pids)

        logger.info("Train pids: {}".format(train_pids))
        logger.info("Valid pids: {}".format(valid_pids))
        logger.info("Test pids: {}".format(test_pids))

        X_train, X_valid, X_test, Y_train, Y_valid, Y_test = [], [], [], [], [], []

        def read_pids_and_append(local_pids, X_list, Y_list):
            for pid in local_pids:
                logger.info("Reading features for {}".format(pid))
                files = glob(os.path.join(args.output, "*_{}*.npy".format(pid)))
                for file in files:
                    filename = file.split('/')[-1]
                    label, major, minor = filename.split('_')
                    if any(map(lambda x: major[:len(x)] == x, rejected_prefixes)):
                        continue
                    data = np.load(file)
                    if data.shape != (1, 8, 874):
                        logger.debug("Patient {} in {} has wrong shape: {}".format(pid, filename, data.shape))
                        continue
                    X_list.append(np.load(file))
                    Y_list.append(int(label))

        read_pids_and_append(train_pids, X_train, Y_train)
        read_pids_and_append(valid_pids, X_valid, Y_valid)
        read_pids_and_append(test_pids, X_test, Y_test)

        X_train = np.concatenate(X_train)
        X_valid = np.concatenate(X_valid)
        X_test = np.concatenate(X_test)
        Y_train = np.array(Y_train)
        Y_valid = np.array(Y_valid)
        Y_test = np.array(Y_test)

        # Shuffle
        X_train, Y_train = shuffle_both(X_train, Y_train)
        X_valid, Y_valid = shuffle_both(X_valid, Y_valid)
        X_test, Y_test = shuffle_both(X_test, Y_test)

        logger.info("Y in train: {}".format(np.sum(Y_train)))
        logger.info("Y in valid: {}".format(np.sum(Y_valid)))
        logger.info("Y in test: {}".format(np.sum(Y_test)))

        return {
            'X_train': X_train,
            'X_valid': X_valid,
            'X_test': X_test,
            'Y_train': Y_train,
            'Y_valid': Y_valid,
            'Y_test': Y_test
        }


class ReadFeaturesAndSplitByPatient(WorkflowTask):
    def _run(self, args, test_size, pids, rejected_prefixes=None, **kwargs):
        """
        :param args: command-line arguments
        :param test_size: this not very precise
        :param rejected_prefixes: prefixes of the recording, which shouldn't be included in train/valid
        :param kwargs:
        :return:
        """
        rejected_prefixes = [] or rejected_prefixes
        pids = map(str, pids)
        out_dir = args.output
        counter = [defaultdict(int), defaultdict(int)]

        files = glob(os.path.join(args.output, "*.npy"))
        for f in files:
            f = f.split("/")[-1]
            l, major, minor = f.split('_')
            counter[int(l)][major[:4]] += 1

        for key in counter[0].keys():
            if not any(map(lambda x: x[:4] == key[:4], pids)):
                counter[0].pop(key)

        test_size = max(1, int(len(counter[0]) * test_size))
        test_pids = counter[0].keys()[:test_size]
        train_pids = counter[0].keys()[test_size:]
        logger.info("Train pids: {}".format(train_pids))
        logger.info("Test pids: {}".format(test_pids))

        X_train, X_test, Y_train, Y_test = [], [], [], []
        def read_pids_and_append(local_pids, X_list, Y_list):
            for pid in local_pids:
                files = glob(os.path.join(args.output, "*_{}*.npy".format(pid)))
                random.shuffle(files)
                for file in files:
                    filename = file.split('/')[-1]
                    label, major, minor = filename.split('_')
                    if any(map(lambda x: major[:len(x)] == x, rejected_prefixes)):
                        continue
                    X_list.append(np.load(file))
                    Y_list.append(int(label))
        read_pids_and_append(train_pids, X_train, Y_train)
        read_pids_and_append(test_pids, X_test, Y_test)

        X_train = np.concatenate(X_train)
        X_test = np.concatenate(X_test)
        Y_train = np.array(Y_train)
        Y_test = np.array(Y_test)

        # Shuffle
        X_train, Y_train = shuffle_both(X_train, Y_train)
        X_test, Y_test = shuffle_both(X_test, Y_test)

        logger.info("Y in train: {}".format(np.sum(Y_train)))
        logger.info("Y in test: {}".format(np.sum(Y_test)))

        return {
            'X_train': X_train,
            'X_test': X_test,
            'Y_train': Y_train,
            'Y_test': Y_test
        }



class ReadFeaturesAndSplit(WorkflowTask):
    def _run(self, args, valid_size, test_size, pids, rejected_prefixes=None, **kwargs):
        rejected_prefixes = rejected_prefixes or []

        X_dict = defaultdict(list)
        X_train, X_valid, X_test, Y_train, Y_valid, Y_test = [], [], [], [], [], []

        for pid in pids:
            logger.info("Getting features from patient %s", str(pid)[:3])
            for path in glob(args.output + "/*_{}*.npy".format(str(pid)[:3])):

                file = path.split('/')[-1]
                file = file.split('.')[0]
                y, major, minor = file.split('_')
                pid = major[:3]
                if any(map(lambda x: major[:len(x)] == x, rejected_prefixes)):
                    continue

                data = np.load(path)
                if data.shape != (1, 8, 874):
                    logger.debug("Patient {} in {} has wrong shape: {}".format(pid, path, data.shape))
                    continue
                X_dict[pid].append((major + '_' + minor, int(y), data))

        for pid in X_dict.keys():
            glist = sorted(X_dict[pid], key=itemgetter(0))
            xlist = map(itemgetter(2), glist)
            ylist = map(itemgetter(1), glist)

            xlist = np.concatenate(xlist)
            ylist = np.array(ylist)

            # TODO - make sure that every hour is in one bin
            # TODO 2 - make sure that number of 0,1 is balanced in every bin (non-trivial!)
            n = xlist.shape[0]
            n_test = int(n * test_size)
            n_valid = int(n * valid_size)
            n_train = n - n_test - n_valid

            X_train.append(xlist[:n_train])
            X_valid.append(xlist[n_train:(n_train + n_valid)])
            X_test.append(xlist[(n_train + n_valid):])
            Y_train.append(ylist[:n_train])
            Y_valid.append(ylist[n_train:(n_train + n_valid)])
            Y_test.append(ylist[(n_train + n_valid):])

        X_train = np.concatenate(X_train)
        X_valid = np.concatenate(X_valid)
        X_test = np.concatenate(X_test)
        Y_train = np.concatenate(Y_train)
        Y_valid = np.concatenate(Y_valid)
        Y_test = np.concatenate(Y_test)

        # Shuffle
        X_train, Y_train = shuffle_both(X_train, Y_train)
        X_test, Y_test = shuffle_both(X_test, Y_test)

        logger.info("Y in train: {}".format(np.sum(Y_train)))
        logger.info("Y in valid: {}".format(np.sum(Y_valid)))
        logger.info("Y in test: {}".format(np.sum(Y_test)))

        return {
            'X_train': X_train,
            'X_valid': X_valid,
            'X_test': X_test,
            'Y_train': Y_train,
            'Y_valid': Y_valid,
            'Y_test': Y_test
        }

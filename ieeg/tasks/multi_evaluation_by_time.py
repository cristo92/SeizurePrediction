from argparse import Namespace

import numpy as np

from daftlearning.logger import logger
from daftlearning.workflow import Workflow
from daftlearning.workflow.workflow_tasks import WorkflowTask

from ieeg.tasks.features_and_splitting import ReadFeaturesAndSplit
from ieeg.tasks.xgboost_classifier import XGBoostClassifier
from ieeg.tasks.cross_validation import CrossValidation
from ieeg.tasks.train_prods import TrainProds
from ieeg.tasks.add_results_to_csv import AddResultsToCSV

class MultiEvaluationByTime(WorkflowTask):
    def __init__(self, *args, **kwargs):
        super(MultiEvaluationByTime, self).__init__(*args, **kwargs)
        self.workflow = Workflow({
            "global_params": {},
            "split_data": {'rejected_prefixes': ['27300', '54800', '44200', '44203']},
            "xgboost": {},
            "cross_validation": {},
            "train_prods": {},
            "csv": {}
        }, "multi_evaluation_workflow") \
            >> ReadFeaturesAndSplit("split_data") \
            >> XGBoostClassifier("xgboost") \
            >> CrossValidation("cross_validation") \
            >> TrainProds("train_prods") \
            >> AddResultsToCSV("csv")

    def _run(self, data_paths, labels, pids, n_runs, metadata_path, **kwargs):
        max_test = 0.4
        step = max_test / n_runs

        for test_size in np.arange(step, max_test, step):
            for data_path, label in zip(data_paths, labels):
                logger.info("Processing {} in {}".format(label, data_path))
                self.workflow.run({
                    "valid_size": 0.3,
                    "test_size": test_size,
                    "label": label,
                    "metadata_path": metadata_path,
                    "args": Namespace(output=data_path, pids=pids),
                    "pids": pids,
                    "test_pids": test_size,
                    "valid_pids": 0.3,
                    "train_pids": 1.0 - test_size - 0.3,
                })

        return {}
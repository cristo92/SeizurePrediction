import os
from shutil import copy
from datetime import datetime

from daftlearning.workflow.workflow_tasks import WorkflowTask
from daftlearning.logger import logger


class PrepareFolders(WorkflowTask):
    def _run(self, args, metadata=None, **kwargs):
        log_folder = metadata or str(datetime.now().strftime("info_%Y-%m-%d-%H:%M"))

        out_path = os.path.join(args.output, log_folder)
        if not os.path.exists(out_path):
            os.mkdir(out_path)

        log_filename = os.path.join(out_path, 'log.txt')
        logger.addFileHandler(log_filename)

        state_path = os.path.join(args.output, "state")
        if not os.path.exists(state_path):
            os.mkdir(state_path)

        copy(args.workflow, out_path)

        return {
            "metadata_path": out_path,
            "state_path": state_path
        }
import os
import shutil

import numpy as np
from daftlearning.logger import logger
from daftlearning.workflow.workflow_tasks import WorkflowTask
from sklearn.metrics import precision_recall_curve, auc, roc_auc_score, log_loss


class CreateRaport(WorkflowTask):
    def _pr_auc(self, y, p):
        precision, recall, _ = precision_recall_curve(y, p)
        return auc(recall, precision)

    def _run(self, args, Y_train, Y_test, preds_test, preds_train, metadata_path, Y_valid=None, preds_valid=None,
             data_info_good=None, data_info_bad=None, **kwargs):
        metrics = {
            'ROC_AUC': roc_auc_score,
            'PREC_REC_AUC': self._pr_auc,
            'LOG_LOSS': log_loss
        }

        raport_filename = os.path.join(metadata_path, 'raport.txt')
        logger.addFileHandler(raport_filename)

        logger.info("Y in train {}/{}".format(np.sum(Y_train), Y_train.shape[0]))
        if Y_valid is not None:
            logger.info("Y in valid {}/{}".format(np.sum(Y_valid), Y_valid.shape[0]))
        logger.info("Y in test  {}/{}".format(np.sum(Y_test), Y_test.shape[0]))

        logger.info("######### TRAIN METRICS #########")
        for name, metric in metrics.items():
            logger.info("{}: {}".format(name, metric(Y_train, preds_train)))

        if Y_valid is not None:
            logger.info("######### VALID METRICS #########")
            for name, metric in metrics.items():
                logger.info("{}: {}".format(name, metric(Y_valid, preds_valid)))

        logger.info("######### TEST  METRICS #########")
        for name, metric in metrics.items():
            logger.info("{}: {}".format(name, metric(Y_test, preds_test)))

        logger.removeHandler(logger.handlers[-1])

        data_info_filename = os.path.join(metadata_path, 'data_info.txt')
        logger.addFileHandler(data_info_filename)

        if data_info_good is not None and len(data_info_good) > 0:
            for pid in data_info_good.keys():
                good = float(data_info_good[pid])
                bad = float(data_info_bad[pid])
                ssum = float(good + bad)
                logger.info("Patient {} good: {:0.2f}% bad: {:0.2f}%".format(pid, 100. * good / ssum, 100. * bad / ssum))
            good = float(sum(data_info_good.values()))
            bad = float(sum(data_info_bad.values()))
            ssum = float(good + bad)
            logger.info("Globally good: {:0.2f}% bad: {:0.2f}%".format(100. * good / ssum, 100. * bad / ssum))

        logger.removeHandler(logger.handlers[-1])

        # Save workflow.
        shutil.copyfile(args.workflow, os.path.join(metadata_path, 'workflow.py'))

        return {
            "roc_auc": roc_auc_score(Y_test, preds_test),
            "prec_rec_auc": self._pr_auc(Y_test, preds_test),
            "log_loss": log_loss(Y_test, preds_test),
        }
from argparse import Namespace

import numpy as np

from daftlearning.logger import logger
from daftlearning.workflow import Workflow
from daftlearning.workflow.workflow_tasks import WorkflowTask

from ieeg.tasks.features_and_splitting import ReadFeaturesAndSplitByPatientWithValid
from ieeg.tasks.xgboost_classifier import XGBoostClassifier
from ieeg.tasks.cross_validation import CrossValidation
from ieeg.tasks.train_prods import TrainProds
from ieeg.tasks.add_results_to_csv import AddResultsToCSV

class MultiEvaluation(WorkflowTask):
    def __init__(self, *args, **kwargs):
        super(MultiEvaluation, self).__init__(*args, **kwargs)
        self.workflow = Workflow({
            "global_params": {},
            "split_data": {"valid_size": 0.0, "test_size": 0.0,
                           'rejected_prefixes': ['27300', '54800', '44200', '44203']},
            "xgboost": {},
            "cross_validation": {},
            "train_prods": {},
            "csv": {}
        }, "multi_evaluation_workflow") \
            >> ReadFeaturesAndSplitByPatientWithValid("split_data") \
            >> XGBoostClassifier("xgboost") \
            >> CrossValidation("cross_validation") \
            >> TrainProds("train_prods") \
            >> AddResultsToCSV("csv")

    def _run(self, data_paths, labels, pids, n_runs, test_size, valid_size, metadata_path, seed, **kwargs):
        n_test = max(1, int(len(pids) * test_size))
        n_valid = max(1, int(len(pids) * valid_size))

        tests = [
            np.array(pids)
            for _ in xrange(n_runs)
        ]

        np.random.seed(seed)
        for a in tests:
            np.random.shuffle(a)


        for i in xrange(n_runs):
            pids = tests[i]
            test_pids = pids[:n_test]
            valid_pids = pids[n_test:(n_test + n_valid)]
            train_pids = pids[(n_test + n_valid):]

            for data_path, label in zip(data_paths, labels):
                logger.info("Processing {} in {}".format(label, data_path))
                self.workflow.run({
                    "test_pids": test_pids,
                    "valid_pids": valid_pids,
                    "train_pids": train_pids,
                    "label": label,
                    "metadata_path": metadata_path,
                    "args": Namespace(output=data_path, pids=pids),
                    "pids": pids,
                })

        return {}
import numpy as np
from daftlearning.workflow.workflow_tasks import WorkflowTask
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler


class ClassifierWithEnsambling(WorkflowTask):
    # WIP
    class Ensambler(object):
        def __init__(self, nmodels=20):
            self.nmodels = nmodels

        def fit(self, X_train, Y_train):
            b, w, n = X_train.shape

            self.masks = [
                np.random.choice(range(n), n * 0.5)
                for _ in xrange(self.nmodels)
            ]

            self.models = [

            ]

            return self

        def predict(self, X_train):
            pass

    def _run(self, X_train, Y_train, **kwargs):
        model = Pipeline([
            (StandardScaler(), 'standard_scaler'),
            (self.Ensambler(), 'ensambler')
        ])

        return {}
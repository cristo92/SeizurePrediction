import numpy as np
from daftlearning.workflow.workflow_tasks import WorkflowTask


class TrainProds(WorkflowTask):
    def _compute_preds(self, X, model):
        b, w, n = X.shape
        preds = np.zeros((b, w))
        for i in xrange(w):
            preds[:, i] = model.predict(X[:, i, :])
        preds = np.sum(preds, axis=1) / w
        return preds

    def _run(self, X_train, model, X_valid=None, **kwargs):

        return {'preds_train': self._compute_preds(X_train, model),
                'preds_valid': self._compute_preds(X_valid, model) if X_valid is not None else None}

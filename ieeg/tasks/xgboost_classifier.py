import numpy as np
import xgboost as xgb
from daftlearning.workflow.workflow_tasks import WorkflowTask
from sklearn.preprocessing import StandardScaler, FunctionTransformer


class XGBoostClassifier(WorkflowTask):
    class PredictPipeline(object):
        def __init__(self, ntree, *args):
            self.pipeline = args
            self.ntree = ntree

        def predict(self, X):
            X = reduce(lambda x, T: T.fit_transform(x), self.pipeline[:-1], X)

            return self.pipeline[-1].predict(X, ntree_limit=self.ntree)

    def _flatten(self, X, Y):
        b, w, n = X.shape

        return (
            X.reshape((b * w, n)),
            np.concatenate([Y.reshape((b, 1))] * w, axis=1).flatten()
        )

    def _run(self, X_train, Y_train, X_test, Y_test, X_valid=None, Y_valid=None, **kwargs):
        st = StandardScaler()

        X_train, Y_train = self._flatten(X_train, Y_train)
        if X_valid is None:
            X_eval, Y_eval = self._flatten(X_test, Y_test)
        else:
            X_eval, Y_eval = self._flatten(X_valid, Y_valid)
        X_train = st.fit_transform(X_train)
        X_eval = st.fit_transform(X_eval)

        xg_train = xgb.DMatrix(X_train, label=Y_train)
        xg_test = xgb.DMatrix(X_eval, label=Y_eval)

        params = {
            'eta': 0.1,
            'min_child_weight': 1,
            'max_depth': 6,
            'gamma': 0,
            'max_delta_step': 0,
            'colsample_bytree': 0.8,
            'colsample_bylevel': 0.8,
            'lambda': 10, #Reg L2
            'alpha': 5, #Reg L1
            'scale_pos_weight': 1,
            # Non boosting params
            'eval_metric': ['logloss', 'auc'],
        }

        model = xgb.train(params, xg_train, evals=[(xg_train, "Train"), (xg_test, "Test")], verbose_eval=True,
                          maximize=True, early_stopping_rounds=5)
        return {'model': self.PredictPipeline(model.best_ntree_limit, StandardScaler(), FunctionTransformer(lambda x: xgb.DMatrix(x)), model)}

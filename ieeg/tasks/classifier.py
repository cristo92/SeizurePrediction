import numpy as np
from daftlearning.workflow.workflow_tasks import WorkflowTask
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from xgboost import XGBClassifier


class Classifier(WorkflowTask):
    def _run(self, X_train, Y_train, gamma, C, **kwargs):
        b, w, n = X_train.shape
        X = X_train.reshape((b * w, n))
        b, = Y_train.shape
        Y = Y_train.reshape((b, 1))
        Y = np.concatenate([Y] * w, axis=1).flatten()

        model = make_pipeline(
            StandardScaler(),
            XGBClassifier(nthread=8, eta=0.1, objective="binary:logistic", reg_lambda=10),
            #RandomForestClassifier(),
            #SVC(gamma=gamma, C=C, probability=True, cache_size=500, random_state=0)
        )
        model.fit(X, Y)

        return {'model': model}
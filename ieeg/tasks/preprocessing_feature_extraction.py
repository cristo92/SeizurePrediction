import os
import json
from datetime import timedelta
from collections import defaultdict

import numpy as np

from daftlearning.workflow.workflow_tasks import WorkflowTask
from daftlearning.logger import logger
from ieeg.database import Database
from ieeg.settings import parse_settings
from ieeg.transformers import Choose10_20Canals
from ieeg.block import Block
from ieeg.parseHeader import DataShapeException


class PreprocessingFeatureExtraction(WorkflowTask):
    def __init__(self, *args, **kwargs):
        super(PreprocessingFeatureExtraction, self).__init__(*args, **kwargs)
        self.fpipeline = None
        self.ppipeline = None

        self.good = defaultdict(int)
        self.bad = defaultdict(int)

        self.DB = Database()

    def _restore_state(self, filename, pids, force):
        self.state = {}
        if not force and os.path.exists(filename):
            with open(filename, "r") as FILE:
                self.state = json.load(FILE)
            for key in self.state.keys():
                self.state[key] = set(self.state[key])

        for pid in pids:
            if str(pid) not in self.state.keys():
                self.state[str(pid)] = set()

    def _dump_state(self, filename):
        for key in self.state.keys():
            self.state[key] = list(self.state[key])

        with open(filename, 'w') as FILE:
            json.dump(self.state, FILE)

    def _bad_data(self, pid, block):
        self.bad[pid] += block['samples']
        return 0, None

    def _proceed_gap(self, block, prev_dataset, prev_length, channels, pid):
        block["gap"] = block["gap"] or 0
        if block["gap"] > 0 and block["gap"] <= 60:
            if prev_dataset is not None:
                # Using Gaussian noise instead of zeros
                noise = np.random.choice([-1, 0, 1], (channels, block["gap"] * 256))
                prev_dataset = np.concatenate((prev_dataset, noise), axis=1)
                prev_length += block["gap"] * 256
        elif block["gap"] > 60:
            self.bad[pid] += prev_dataset.shape[1] if prev_dataset is not None else 0
            prev_dataset = None
            prev_length = 0

        return block, prev_dataset, prev_length

    def _give_label(self, pid, ts_end):
        if self.DB.scan(pid, ts_end, ts_end + timedelta(0, 3600)) == Database.SEIZURE:
            logger.debug("There is seizure: {}".format(ts_end))
            return 1
        elif self.DB.scan(pid, ts_end, ts_end + timedelta(0, 4 * 3600)) == Database.NON_SEIZURE:
            return 0
        else:
            return -1

    def _save_dataset(self, X, Y, block_id, i):
        if Y == 1:
            np.save(parse_settings().output + "/1_" + str(block_id) + "_" + str(i), X)
        elif Y == 0:
            np.save(parse_settings().output + "/0_" + str(block_id) + "_" + str(i), X)

    def _produce_features_from_block(self, pid, block, recording, prev_length, prev_dataset):
        """
        It gets observation from block and prev_dataset and save them on disk
        If some data remains unused it is returned as pair (prev_length, prev_dataset)

        :param pid:
        :param block: dict, from database (keys == schema)
        :param recording: dict, from database (keys == schema)
        :param prev_length: length of prev_dataset
        :param prev_dataset: part of data from previous block, which can be used in this block
        :return: (prev_length, prev_dataset), which can be used for proceeding next block
                 if the data is corrupted or sth wrong we return (0, None) - which means nothing from this
                 block can be used for the next one
        """
        header = self.DB.get_header(block["eeg_file"])

        # Channel_chooser-specific checks
        if isinstance(self.channel_chooser, Choose10_20Canals) and \
                        len(set(header['elec_names']).intersection(Choose10_20Canals.surface_elecs)) < 19:
            logger.info("Block {} doesn't have surface electrodes.".format(block['id']))
            return self._bad_data(pid, block)

        logger.info("Processing {} Block {} to {} from {} Recording.". \
                     format(block['id'], block['begin'], block['end'], block['recording']))

        try:
            bblock = Block(block, recording["channels"], recording["sample_rate"])
        except IOError as e:
            logger.info("Block {} in recording {} with error {}".format(block['id'], block['recording'], e))
            return self._bad_data(pid, block)
        except DataShapeException:
            logger.info("Block {} shape doesn't match no_channels and no_samples".format(block['id']))
            return self._bad_data(pid, block)

        block, prev_dataset, prev_length = self._proceed_gap(block, prev_dataset, prev_length,
                                                             recording['channels'], pid)

        # Some blocks have only few seconds - so we want to concatenate them to prev_dataset
        missing_length = 600 * 256 - prev_length
        if bblock.length() < missing_length:
            prev_length += bblock.length()
            prev_dataset = np.concatenate((prev_dataset, bblock.data),
                                          axis=1) if prev_dataset is not None else bblock.data
            return (prev_length, prev_dataset)

        for i in xrange(10000):
            ndarray, offset = bblock.dataset(missing_length, prev_dataset)
            n, m = ndarray.shape
            if m == 0:
                break
            prev_dataset = None
            missing_length = 600 * 256
            ts_end = block["begin"] + timedelta(0, offset / 256.)
            Y = self._give_label(pid, ts_end)

            if Y > -1:
                ppipeline = self.channel_chooser(header)
                X = ppipeline.fit_transform(ndarray)
                n, m = X.shape
                if n < 19:
                    logger.info("Ommiting block {} due to lack of 10_20 electrodes. We had only {}".format(
                        block['id'], n
                    ))
                    return self._bad_data(pid, block)
                X.resize((1, n, m))

                try:
                    X = self.fpipeline.fit_transform(X)
                except np.linalg.linalg.LinAlgError as e:
                    # This can be possibly caused with adding zeros for gaps - it can give us problems with
                    # eigenvalues
                    self.bad[pid] += 600 * 256
                    logger.info("Error in feature extraction: {}".format(e))
                    continue
                except ValueError as e:
                    # This has not been investigated yet
                    self.bad[pid] += 600 * 256
                    logger.info("Error in feature extraction: {}".format(e))
                    continue

                self._save_dataset(X, Y, block['id'], i)
                logger.info("Good data in {}".format(block['id']))
                self.good[pid] += 600 * 256
            else:
                logger.info("Too big holes or seizure nearby in data to give label in {}".format(block['id']))
                self.bad[pid] += 600 * 256
            if bblock.length() < 600 * 256:
                break
        prev_length = bblock.length()
        prev_dataset = bblock.remaining_dataset()

        return prev_length, prev_dataset

    def _produce_features(self, pid, block_dict):
        recordings = self.DB.get_recordings(pid)
        for recording in recordings:
            block_nr = block_dict.get(pid, None)
            blocks = self.DB.get_blocks(recording["id"], block_nr)
            prev_length = 0
            prev_dataset = None

            for block in blocks:
                if block['id'] in self.state[str(pid)]:
                    continue
                prev_length, prev_dataset = self._produce_features_from_block(pid, block, recording, prev_length,
                                                                              prev_dataset)
                self.state[str(pid)].add(block['id'])

    def _run(self, feature_extraction_pipeline, pids, state_path, channel_chooser, force=False, block_nr=None, execute=True, **kwargs):
        if not execute:
            return

        self.channel_chooser = channel_chooser

        metadata_filename = os.path.join(state_path, "{}_status.meta".format(self.__class__.__name__))
        self._restore_state(metadata_filename, pids, force)
        logger.info(self.state)

        try:
            self.fpipeline = feature_extraction_pipeline
            for pid in pids:
                self._produce_features(pid, block_nr)
        except KeyboardInterrupt:
            logger.info("KeyboardInterrupt.")
        finally:
            self._dump_state(metadata_filename)

        if force:
            for pid in self.good.keys():
                good = float(self.good[pid])
                bad = float(self.bad[pid])
                ssum = float(good + bad)
                logger.info("Patient {} good: {:0.2f}% bad: {:0.2f}%".format(pid, 100. * good / ssum, 100. * bad / ssum))
            good = float(sum(self.good.values()))
            bad = float(sum(self.bad.values()))
            ssum = float(good + bad)
            logger.info("Globally good: {:0.2f}% bad: {:0.2f}%".format(100. * good / ssum, 100. * bad / ssum))

        return {
            'data_info_good': self.good, 'data_info_bad': self.bad
        }
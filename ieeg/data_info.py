import operator

from dbQueries import get_seizures_by_patient, get_notseizures_by_patient, sql_get

# Attributes in database
observation_header0 = (
    'block_id', 'file_path', 'file_name', 'file_id'
)
observation_header1 = (
    'file_path', 'file_name', 'file_id',
    'seizure_eeg_onset', 'seizure_eeg_offset',
    'seizure_clin_onset', 'seizure_clin_offset',
    'seizure_pattern', 'seizure_classification',
    'seizure_vigilance', 'seizure_id',
    'block_recording'
)

def get_patient_pids():
    return [108402, 112502, 27302, 54802, 58302, 62002, 95802,
            107702, 109602, 11502, 44202, 56502, 59002, 92202,
            97002]

def get_local_patient_pids():
    return [11502, 27302, 44202, 54802, 56502, 58302, 59002, 62002]

# Attributes in file
head_keys = (
    'elec_names', 'adm_id', 'pat_id', 'sample_freq', 
    'start_ts', 'duration_in_sec', 'rec_id', 
    'conversion_factor', 'num_channels', 
    'num_samples', 'sample_bytes'
)
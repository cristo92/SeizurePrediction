import os
import numpy as np

interesting_channels = \
    ['G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G7', 'G8', 'G9', 'G10', 'G11', 'G12', 'G13', 'G14', 'G15', 'G16',
     'G17', 'G18', 'G19', 'G20', 'G21', 'G22', 'G23', 'G24', 'G25', 'G26', 'G27', 'G28', 'G29', 'G30', 'G31', 'G32',
     'TP1', 'TP2', 'TP3', 'TP4',
	 'TBA1', 'TBA2', 'TBA3', 'TBA4',
     'TBB1', 'TBB2', 'TBB3', 'TBB4']

def path_join(*args):
	def remove_leading(x):
		while x[0] == '/':
			x = x[1:]
		return x

	args = list(args)
	args[1:] = map(remove_leading, args[1:])

	return os.path.join(*args)

def hash_list(strings):
	return hash("_".join(sorted(strings)))

def shuffle_both(X, Y):
    # Shuffle
    idx = np.arange(X.shape[0])
    np.random.shuffle(idx)
    return X[idx], Y[idx]
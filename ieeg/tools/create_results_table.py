from ieeg.database import Database

if __name__ == "__main__":
    DB = Database()
    qcreate_database = """
    CREATE TABLE results (
        preprocessing TEXT,
        classifier TEXT,
        pid TEXT,
        log_loss REAL,
        prec_rec_auc REAL,
        roc_auc REAL,
        logpath TEXT,
        PRIMARY KEY (preprocessing, classifier, pid)
    );
    """
    result = DB.sql_insert(qcreate_database)
    print result
from collections import defaultdict
from operator import itemgetter

from ieeg.parseHeader import read_head
from ieeg.settings import parse_settings
from ieeg.data import Data
from ieeg.dbQueries import get_notseizures_by_patient, get_seizures_by_patient
from ieeg.utils import path_join

def my_hash(strings):
    return str(hash("_".join(sorted(strings))))

if __name__ == "__main__":
    args = parse_settings()
    for pid in args.pids:
        try:
            electrodes_dict = {}

            seizures0 = get_notseizures_by_patient(pid) 
            seizures1 = get_seizures_by_patient(pid, all_=True)

            seizures = sorted(seizures0 + seizures1, key=itemgetter('file_name'))
            headers = map(lambda s: read_head(path_join(args.path, s['file_path'], 
                                                        s['file_name'])), 
                          seizures)

            for head, s in zip(headers, seizures):
                key = my_hash(head['elec_names']) + "_" + str(head['sample_freq'])
                if key not in electrodes_dict:
                    electrodes_dict[key] = [0, 0]
                electrodes_dict[key][0] += 1
                electrodes_dict[key][1] += int('seizure_eeg_onset' in s)

            print "################### {} ###################".format(pid)
            for v, k in electrodes_dict.items():
                print v, k
        except IOError:
            pass
import ast
from collections import defaultdict

from ieeg.database import Database
from ieeg.transformers import Choose10_20Canals

if __name__ == "__main__":
    DB = Database()

    headers = ["id", "recording", "start", "stop"]

    q_seizures = """
    SELECT id, recording, eeg_onset, eeg_offset FROM seizure"""
    q_subclinical = """
    SELECT id, recording, onset, offset FROM subclinicalevent"""

    seizures = map(lambda x: dict(zip(headers, x)), list(DB.sql_get(q_seizures)))
    #seizures += map(lambda x: dict(zip(headers, x)), list(DB.sql_get(q_subclinical)))

    surface_seiz = defaultdict(int)
    total_seiz = defaultdict(int)

    for s in seizures:
        q_elec_names = """
        SELECT elec_names, pat_id FROM headers WHERE file_id IN
        (SELECT eeg_file FROM block WHERE begin < "{}" ORDER BY begin DESC LIMIT 1)
        """.format(s['start'])

        elec_names = list(DB.sql_get(q_elec_names))
        if len(elec_names) > 0:
            elec_names = list(elec_names[0])
            elec_names[0] = ast.literal_eval(elec_names[0][1:-1])
            if set(elec_names[0]).issuperset(Choose10_20Canals.surface_elecs):
                surface_seiz[elec_names[1]] += 1
            total_seiz[elec_names[1]] += 1

    for pid in sorted(total_seiz.keys()):
        print "Patient {}: {}/{}".format(pid, surface_seiz[pid], total_seiz[pid])
from ieeg.database import Database
from ieeg.data_info import get_local_patient_pids, get_patient_pids

if __name__ == "__main__":
    DB = Database()
    sql = """
    SELECT * FROM results
    """

    results = [
        {
            'preprocessing': x[0],
            'classifier': x[1],
            'pid': x[2],
            'log_loss': x[3],
            'prec_rec_auc': x[4],
            'roc_auc': x[5],
            'logpath': x[6]
        }
        for x in list(DB.sql_get(sql))
    ]


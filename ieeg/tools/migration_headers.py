import sqlite3
from operator import itemgetter

from ieeg.database import Database
from ieeg.data_info import get_patient_pids
from ieeg.parseHeader import read_head_from_seizure
from ieeg.dbQueries import get_seizures_by_patient, get_notseizures_by_patient
from ieeg.settings import parse_settings

if __name__ == "__main__":
    db = Database()

    try:
        for pid in parse_settings().pids:
            q_files = """
            SELECT id, name, path FROM files
            WHERE format = "raw" AND admission IN
            (SELECT id FROM admission WHERE patient = {})
            """.format(pid)

            mfiles = db.sql_get(q_files)
            mfiles = map(lambda x: dict(zip(['file_id', 'file_name', 'file_path'], x)), mfiles)
            mfiles = sorted(mfiles, key=itemgetter('file_id'))
            print map(itemgetter('file_id'), mfiles)

            for s in mfiles:
                print s
                try:
                    head = read_head_from_seizure(s)
                    head['file_id'] = s['file_id']
                    db.add_header(**head)
                except sqlite3.IntegrityError:
                    print head
    except IOError:
        print "IOERROR"
        pass


    db.commit();

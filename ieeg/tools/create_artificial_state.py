import glob
import os
import json
from collections import defaultdict

from ieeg.settings import parse_settings

def fpid(x):
    return x[:4] + "2"

if __name__ == "__main__":
    args = parse_settings()

    metadir = os.path.join(args.output, "state")
    if not os.path.exists(metadir):
        os.mkdir(metadir)
    outfile = os.path.join(metadir, "PreprocessingFeatureExtraction_status.meta")
    state = defaultdict(list)
    files = glob.glob(os.path.join(args.output, "*.npy"))
    for file in files:
        file = file.split("/")[-1]
        label, major, minor = file.split("_")
        state[fpid(major)].append(int(major))

    with open(outfile, "w") as F:
        json.dump(state, F)
from operator import itemgetter

from ieeg.dbQueries import get_seizures_by_patient, get_notseizures_by_patient
from ieeg.parseHeader import read_head_from_seizure


def generate_intervals_per_patient(pid):
	seizures0 = get_notseizures_by_patient(pid)
	seizures1 = get_seizures_by_patient(pid)

	sh = map(lambda x: x.update(read_head_from_seizure(x)), seizures1 + seizures0)
	sh = sorted(sh, key=itemgetter('start_ts'))
# encoding=utf-8
import os
import sys
from importlib import import_module

from keras.backend.tensorflow_backend import set_session
import tensorflow as tf

from ieeg.settings import parse_settings


if __name__ == '__main__':
    # Setting limit for GPU resources
    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 0.3
    set_session(tf.Session(config=config))


    args = parse_settings(sys.argv[1:])

    if not os.path.exists(args.output):
        os.makedirs(args.output)

    try:
        params_module = import_module(args.workflow)
    except ImportError:
        path = '.'.join(args.workflow.split('/'))
        if path[-3:] == '.py':
            path = path[:-3]
        params_module = import_module(path)

    params_module.workflow.run()

# encoding=utf-8
import sys
from datetime import datetime, timedelta
from collections import defaultdict
from operator import itemgetter

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from daftlearning.logger import logger

from ieeg.dbQueries import (get_notseizures_by_patient, get_seizures_by_patient,
                       get_electrodes, get_propagation)
from ieeg.parseHeader import read_data, read_head
from ieeg.utils import path_join, hash_list
from ieeg.settings import parse_settings


class MetaData(object):
    def __init__(self, patient_id):
        self.electrodes_set = set() #superset of electrodes
        self.electrodes_inter = None #intersection of electrodes
        self.electrodes_name = None
        self.origin_set = set() #superset of origin electrodes
        self.seizures = []
        #TODO - generate self.freq instead of hardcode
        self.freq = 256

        self.fit(patient_id)

    def was_seizure(self, start_ts, end_ts):
        for s in self.seizures:
            if s['seizure_clin_onset'] < end_ts and \
                    s['seizure_clin_offset'] > start_ts:
                return True
        return False

    def fit(self, patient_id):
        self.seizures = get_seizures_by_patient(patient_id)
        non_seizures = get_notseizures_by_patient(patient_id)

        # Fit electrodes_set and electrodes_inter
        for s in self.seizures + non_seizures:
            elecs = get_electrodes(s['block_recording'])
            elec_names = set(map(itemgetter('electrode_name'), elecs))
            self.electrodes_set.update(elec_names)
            if self.electrodes_inter is None:
                self.electrodes_inter = elec_names
            self.electrodes_inter.intersection_update(elec_names)

        # Fit origins set
        # Origins are the names of electrodes, which where closest
        # to the seizure outbreak.
        for s in self.seizures:
            origins = get_propagation(s['seizure_id'])
            self.origin_set.update(set(origins))

        # Fit electrodes_name
        for s in non_seizures:
            elecs = get_electrodes(s['block_recording'])
            elec_names = map(itemgetter('electrode_name'), elecs)
            if len(elec_names) == len(self.electrodes_set):
                if self.electrodes_name is None:
                    self.electrodes_name = elec_names
                assert self.electrodes_name == elec_names


 
class Data(object):
    def seizure_to_head(self, s):
        return read_head(path_join(self.path, s['file_path'], s['file_name']))

    def get_raport(self):
        seizures_remained = self.raport['seizures_available'] - \
                            self.raport['lost_due_to_elec_missmatch'] - \
                            self.raport['lost_due_to_lack_of_data'] - \
                            self.raport['lost_due_to_seizure_inside']
        return """
        We've got {seizures} seizures. We've lost 
        {lost_due_to_elec_missmatch} because of electrodes missmatch,
        {lost_due_to_lack_of_data} due to lack of data and
        {lost_due_to_seizure_inside} due to seizure inside of
        {seizures_available} total seizures at the begining.
        """.format(seizures=seizures_remained, **self.raport)

    def __init__(self, pid, path, type, batch_size=16, observation_time=60 * 10, 
                 margin_time=60 * 10):
        self.pid = pid
        self.path = path
        assert type in {'detection', 'prediction'}
        self.type = type
        self.bsize = batch_size
        self.observation_time = observation_time
        self.margin_time = margin_time
        self.seizures = None
        self.metadata = MetaData(pid)
        self.raport = {
            'seizures_available': 0,
            'lost_due_to_elec_missmatch': 0,
            'lost_due_to_lack_of_data': 0,
            'lost_due_to_seizure_inside': 0,
        }

        seizures0 = get_notseizures_by_patient(self.pid) 
        seizures1 = get_seizures_by_patient(self.pid, all_=True)

        seizures = sorted(seizures0 + seizures1, key=itemgetter('file_name'))
        headers = map(lambda s: self.seizure_to_head(s), seizures)
        self.seizures_and_headers = zip(seizures, headers)
        self.raport['seizures_available'] = len(seizures)

        start_ts = None
        freq = None
        elec_names = None

        ########################################################################
        # Chosing one set of elec_names
        ########################################################################
        counter = defaultdict(int)
        for s, h in self.seizures_and_headers:
            counter[hash_list(h['elec_names'])] += int('seizure_eeg_onset' in s)
        dominant_elecs_hash, _ = max(counter.items(), key=itemgetter(1))

        self.seizures_and_headers = filter(
            lambda (s,h): hash_list(h['elec_names']) == dominant_elecs_hash,
            self.seizures_and_headers
        )

        seizures_remained = len(filter(
            lambda s: hash_list(self.seizure_to_head(s)['elec_names']), 
            seizures1
        ))
        logger.info("There should be {} seizures.".format(seizures_remained))
        self.raport['lost_due_to_elec_missmatch'] = len(seizures) - seizures_remained

        ########################################################################
        # Checking assumptions about data.
        ########################################################################
        for s, head in self.seizures_and_headers:
            head = read_head(path_join(self.path, s['file_path'], 
                                       s['file_name']))
            if start_ts is not None:
                try:
                    assert start_ts < head['start_ts']
                except AssertionError:
                    logger.error('Sorting by file_name isn\'t equal '
                                  'sorting by time start. Recording id {}' \
                                  .format(s['block_recording']))
            if freq is None:
                freq = head['sample_freq']
            try:
                assert freq == head['sample_freq']
            except AssertionError:
                logger.error("Different frequency in {} {}s" \
                              .format(s['file_name'], head['sample_freq']))
            if elec_names is None:
                elec_names = set(head['elec_names'])
            try:
                assert elec_names == set(head['elec_names'])
            except AssertionError:
                logger.error('Different elec_names in {} First not in sec {}.'
                              'Sec not in first {}'\
                              .format(s['block_recording'],
                                      elec_names - set(head['elec_names']), 
                                      set(head['elec_names']) - elec_names))

            start_ts = head['start_ts']

        self.freq = freq
        self.observation_points = freq * self.observation_time
        self.margin_points = freq * self.margin_time
        self.elec_names = elec_names
        # Buffer is np.array, which last vector represents 'Y'
        self.buffer = np.array([]).reshape((len(self.elec_names) + 1, 0))
        self.buffer_ptr = 0
        self.recording_ptr = iter(self.seizures_and_headers)
        self.prev_head = None

    def __iter__(self):
        return self

    def _add_recording_to_buffer(self):
        seizure, head = self.recording_ptr.next()

        head, X = read_data(path_join(self.path, seizure['file_path'], 
                                      seizure['file_name']))
        logger.info("Read new file: {} of {} (from {} to {})".format(
            seizure['file_name'], seizure['block_recording'], head['start_ts'], 
            head['start_ts'] + timedelta(0, int(head['duration_in_sec']))))
        # Adding labels as the last row to data
        X = np.concatenate((X, np.zeros((1, X.shape[1]))))

        n = head['num_samples']

        if 'seizure_clin_onset' in seizure.keys():
            logger.info("Hey there is a seizure inside {}".format(
                seizure['file_name']))
            observation_start = head['start_ts']
            observation_end  = observation_start + \
                                timedelta(0, int(head['duration_in_sec']))
            try:
                seizure_start = datetime.strptime(seizure['seizure_clin_onset'], 
                                                  "%Y-%m-%d %H:%M:%S.%f")
            except ValueError:
                seizure_start = datetime.strptime(seizure['seizure_clin_onset'], 
                                                  "%Y-%m-%d %H:%M:%S")
            try:
                seizure_end = datetime.strptime(seizure['seizure_clin_offset'], 
                                                "%Y-%m-%d %H:%M:%S.%f")
            except ValueError:
                seizure_end = datetime.strptime(seizure['seizure_clin_offset'], 
                                                "%Y-%m-%d %H:%M:%S")

            sec_off = (seizure_start - observation_start).total_seconds()
            spoint = int(n * (sec_off / float(head['duration_in_sec'])))
            epoint = int(n * (sec_off / float(head['duration_in_sec'])))
            X[-1][spoint:epoint] = 1

        ending = head['start_ts']
        if self.prev_head is not None:
            ending = self.prev_head['start_ts'] + \
                        timedelta(0, int(self.prev_head['duration_in_sec']))
        if head['start_ts'] - ending > timedelta(0, 1):
            delta = (head['start_ts'] - ending).total_seconds()
            X = np.concatenate([
                    np.zeros((len(self.elec_names), int(delta * self.freq))), 
                    np.full((1, int(delta * self.freq)), -1, dtype=np.float32)
                ], 
                axis=0)
        self.buffer = np.concatenate((self.buffer, X), axis=1)

        self.prev_head = head

    def next(self):
        bsize = self.bsize
        batch_xs = []
        batch_ys = []

        while bsize > 0:
            while self.buffer.shape[1] - self.buffer_ptr < self.observation_points + self.margin_points:
                self.buffer = np.delete(self.buffer, np.s_[:self.buffer_ptr], axis=1)
                self.buffer_ptr = 0
                self._add_recording_to_buffer()
            logger.debug("({}, {}) {}s {}".format(
                self.buffer.shape[0], self.buffer.shape[1] - self.buffer_ptr, 
                (self.buffer.shape[1] - self.buffer_ptr) / self.freq, 
                self.observation_points))
            # TODO more efficiently remove empty spaces
            this_obs = self.buffer[:, :self.observation_points]
            self.buffer_ptr += self.observation_points
            following_obs = self.buffer[:, self.buffer_ptr:self.buffer_ptr + self.margin_points]

            # Remove observation if it has missing parts
            if np.min(this_obs[-1]) == -1:
                if np.max(following_obs[-1]) == 1:
                    logger.warning("We are ommiting 1 label, because of missing"
                                    " part {}% :(".format(100. * this_obs.shape[1] / np.sum(this_obs[-1] == -1)))
                    self.raport['lost_due_to_lack_of_data'] += 1
                continue
            if self.type == 'detection':
                if np.max(this_obs[-1]) == 1:
                    batch_ys.append(np.array([1]))
                    obs = np.expand_dims(this_obs[:-1], axis=0)
                    batch_xs.append(obs)
                else:
                    batch_ys.append(np.array([0]))
                    obs = np.expand_dims(this_obs[:-1], axis=0)
                    batch_xs.append(obs)
            if self.type == 'prediction':
                # Remove observation if it is seizure one
                if np.max(this_obs[-1]) == 1:
                    if np.max(following_obs[-1]) == 1:
                        logger.warning("We are ommiting 1 label, because of seizure"
                                      " inside :(")
                        self.raport['lost_due_to_seizure_inside'] += 1
                    continue
                if np.max(following_obs[-1]) == 1:
                    batch_ys.append(np.array([1]))
                    obs = np.expand_dims(this_obs[:-1], axis=0)
                    batch_xs.append(obs)
                elif np.min(following_obs[-1]) == 0:
                    batch_ys.append(np.array([0]))
                    obs = np.expand_dims(this_obs[:-1], axis=0)
                    batch_xs.append(obs)
                else:
                    continue

            bsize -= 1

        return np.concatenate(batch_xs, axis=0), \
               np.concatenate(batch_ys, axis=0)


if __name__ == "__main__":
    args = parse_settings(sys.argv[1:])
    data_context = Data(11502, args.path)
    #data_context.visualize()
    
    X, Y = data_context.next_batch(14)
    print X.shape
    print X[0].shape
    print len(Y)


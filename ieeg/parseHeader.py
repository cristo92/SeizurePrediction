#!/usr/bin/python
#-*- coding: utf-8 -*-
import numpy as np
import datetime
from daftlearning.logger import logger
from ieeg.utils import path_join
from ieeg.settings import parse_settings

def read_head_from_seizure(s):
    path = parse_settings().path
    filepath = path_join(path, s['file_path'], s['file_name'])
    print filepath
    return read_head(filepath)

def read_head(file_):
    """
    Reading epilepsy database head file
    """
    if file_[-5:] == ".head" or file_[-5:] == ".data":
        file_ = file_[:-5]
    f = open(file_+".head", 'r')
    to_ret = {}
    for line in f.readlines():
        b = line.split('=')
        key, value = line.split('=')
        value = value.strip()
        if key in ['num_samples', 'sample_freq', 'sample_bytes']:
            value = int(value)
        elif key in ['duratin_in_sec', 'conversion_factor']:
            value = float(value)
        elif key == 'elec_names':
            value = value[1:-1].split(',')
        elif key == 'start_ts':
            value = datetime.datetime.strptime(value, "%Y-%m-%d %H:%M:%S.%f")
        to_ret.update({key: value})
    return to_ret

class DataShapeException(Exception):
    pass

def read_data(file_, in_float=False):
    """
    Reading epilepsy database data file
    """
    if file_[-5:] == ".head" or file_[-5:] == ".data":
        file_ = file_[:-5]
    head = read_head(file_)
    data_type = np.int16 if head['sample_bytes'] == 2 else np.int32
    data = np.fromfile(file_+".data", dtype=data_type)
    if in_float:
        data = np.float_(data)
        data *= head['conversion_factor']
    no_channels = len(head['elec_names'])
    no_samples = head['num_samples']
    try:
        return head, np.reshape(data, (no_channels, no_samples), 'F')
    except ValueError as e:
        raise DataShapeException

def read_channels(file_, channels=None, in_float=False):
    """
    Do not read all data from file, but a selected channels
    """
    if file_[-5:] == ".head" or file_[-5:] == ".data":
        file_ = file_[:-5]
    head = read_head(file_)
    data_type = np.int16 if head['sample_bytes'] == 2 else np.int32
    channels_ = head['elec_names']
    if channels is None:
        channels = range(len(channels_))
    no_samples = head['num_samples']
    to_ret = np.zeros([len(channels), no_samples], dtype=data_type)
    with open(file_+".data", 'r') as fid:
        for i, ch in enumerate(channels):
            ch_str = ""
            if isinstance(ch, int):
                ch_idx = ch
            elif isinstance(ch, str):
                ch_idx = channels_.index(ch)
            else:
                raise ValueError("Wrong type of channels")
            fid.seek(ch_idx*head['sample_bytes'], 0)
            for j in range(no_samples):
                ch_str += fid.read(head['sample_bytes'])
                fid.seek(no_samples*head['sample_bytes'], 1)
            to_ret[i] = np.fromstring(ch_str, dtype=data_type)
    return to_ret

if __name__ == '__main__':
    head = read_head('test.head')

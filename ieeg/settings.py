import argparse
import logging
import sys
import os
from datetime import datetime

from daftlearning.logger import logger
from ieeg.data_info import get_local_patient_pids, get_patient_pids

def parse_settings(argv=None):
	if argv is None:
		argv = sys.argv[1:]
	parser = argparse.ArgumentParser()
	parser.add_argument('-p', '--path', type=str, 
		                help='Path to pat_* folders i.e. folder with input data',
		                default='/Users/krzysztof/work/SeizurePrediction/')
	parser.add_argument('-l', '--logging', type=str, default='INFO', 
		                help='Logging level.')
	parser.add_argument('-mp', '--multiprocessing', action='store_true')
	parser.add_argument('-o', '--output', default=None, 
		                help='Folder for outputing data.')
	parser.add_argument('--pids', default=get_patient_pids(), nargs='+', type=int,
		                help='Pids of patients.')
	parser.add_argument('-w', '--workflow', type=str,
						help='Path to workflow.')
	parser.add_argument('-f', '--force', default=False, action='store_true',
						help='Recompute all features for all blocks.')
	parser.add_argument('-m', '--metadata', default=None,
						help="Name of the folder conatining logs.")

	argspace = parser.parse_args(argv)
	logger.setLevel(argspace.logging)

	return argspace

def get_path():
	args = parse_settings(sys.argv[1:])
	return args.path
import unittest
from datetime import datetime
from dateutil.parser import parse

from ieeg.database import Database

class TestSeizureInInterval(unittest.TestCase):
    def setUp(self):
        self.db = Database()

    def test44202(self):
        pass
        """
        Easy one: i.e. seizure is inside file
        it comes from patient 44202
        file_id=44200003102
        start_ts = '2005-05-23 21:14:41.000000'
        seizure_start = u'2005-05-23 21:54:14.325195
        seizure_end = u'2005-05-23 21:56:31.628906'
        """
        self.assertEqual(self.db.SEIZURE, self.db.scan(44202, datetime(2005, 5, 23, 21, 15), datetime(2005, 5, 23, 21, 57)))
        self.assertEqual(self.db.NON_SEIZURE, self.db.scan(44202, datetime(2005, 5, 23, 21, 15), datetime(2005, 5, 23, 21, 54)))
        """
        file_id=44200001102
        start_ts = '2005-05-23 20:06:31.000000'
        """
        self.assertEqual(self.db.NON_SEIZURE, self.db.scan(44202, datetime(2005, 5, 23, 20 ,6, 31), datetime(2005, 5, 23, 21, 6, 31)))
        """
        There is nothing between those two files
        """
        self.assertEqual(self.db.UNKNOWN, self.db.scan(44202, datetime(2005, 5, 23, 21, 6, 31), datetime(2005, 5, 23, 21, 17)))

    def test27302(self):
        pass
        """
        file_id = 27301017102
        'seizure_eeg_offset': u'2004-05-01 04:23:19.589844',
        'seizure_eeg_onset': u'2004-05-01 04:21:26.613281',
        'start_ts' = '2004-05-01 04:00:07.000000'
        """
        self.assertEqual(self.db.SEIZURE, self.db.scan(27302, datetime(2004, 5, 1, 4, 1), datetime(2004, 5, 1, 4, 24)))
        self.assertEqual(self.db.NON_SEIZURE, self.db.scan(27302, datetime(2004, 5, 1, 4, 1), datetime(2004, 5, 1, 4, 21, 0)))
        """
        Crossing:
        file_id=27301018102
        start_ts='2004-05-01 05:00:08.000000'
        """
        self.assertEqual(self.db.NON_SEIZURE, self.db.scan(27302, datetime(2004, 5, 1, 4, 30), datetime(2004, 5, 1, 5, 30)))
        self.assertEqual(self.db.SEIZURE, self.db.scan(27302, datetime(2004, 5, 1, 4, 1), datetime(2004, 5, 1, 5, 30)))
        """
        file_id=27300001102
        start_ts='2004-04-26 20:34:28.000000'
        """
        self.assertEqual(self.db.NON_SEIZURE, self.db.scan(27302, datetime(2004, 4, 26, 20, 35), datetime(2004, 4, 26, 20, 50)))

    def test_58702_107302(self):
        self.assertEqual(self.db.UNKNOWN,
                         self.db.scan(58702, parse("2008-12-01 20:19:28.983398"), parse("2008-12-01 20:19:30.983398")))
        self.assertEqual(self.db.UNKNOWN,
                         self.db.scan(58702, parse("2008-12-01 20:21:24"), parse("2008-12-01 20:23:24")))
        self.assertEqual(self.db.SEIZURE,
                         self.db.scan(107302, parse("2008-12-01 20:19:28.983398"), parse("2008-12-01 20:19:30.983398")))
        self.assertEqual(self.db.SEIZURE,
                         self.db.scan(107302, parse("2008-12-01 20:21:24"), parse("2008-12-01 20:23:24")))
        self.assertEqual(self.db.SEIZURE,
                         self.db.scan(107302, parse("2008-12-01 20:19:30.983398"), parse("2008-12-01 20:21:24")))
        self.assertEqual(self.db.NON_SEIZURE,
                         self.db.scan(107302, parse("2008-12-01 20:19:27.983398"), parse("2008-12-01 20:19:28.983398")))

    def test_subclinicalevent(self):
        self.assertEqual(self.db.SUBCLINICAL,
                         self.db.scan(107302, parse("2008-12-02 16:24:08.458008"), parse("2008-12-02 16:25:37.183594")))

if __name__ == '__main__':
	unittest.main()
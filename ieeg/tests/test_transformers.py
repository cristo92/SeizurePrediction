import unittest
import sys

import numpy as np
from sklearn.pipeline import Pipeline, make_pipeline

from seizure_prediction.transforms import PhaseSynchrony as PhaseSynchronyMike

from ieeg.transformers import (Windower, Correlation, FeatureUnion,
                               PSDSumPerBin, ShannonEntropy, PhaseSynchrony)
from ieeg.parseHeader import read_data

class TestTransformers(unittest.TestCase):
    def test_windower(self):
        T = Windower(8)
        A = np.array([[[1, 2, 3, 4, 5, 6, 7, 8]]])
        B = T.fit_transform(A)
        self.assertEqual(B.shape, (1, 8, 1, 1))
        diff = B == np.array([[[[1]], [[2]], [[3]], [[4]], [[5]], [[6]], [[7]], [[8]]]])
        self.assertTrue(np.all(diff))

    def test_windower2(self):
        T = Windower(8)
        A = np.array([[[1, 2, 3, 4, 5, 6, 7, 8],
                       [-1, -2, -3, -4, -5, -6, -7, -8]]])
        self.assertEqual(A.shape, (1, 2, 8))
        B = T.fit_transform(A)
        self.assertEqual(B.shape, (1, 8, 2, 1))
        diff = B == np.array([[[[1], [-1]], [[2], [-2]], [[3], [-3]], [[4], [-4]],
                               [[5], [-5]], [[6], [-6]], [[7], [-7]], [[8], [-8]]]])
        self.assertTrue(np.all(diff))

    def test_windower3(self):
        T = Windower(4)
        A = np.array([[[1, 2, 3, 4, 5, 6, 7, 8],
                       [-1, -2, -3, -4, -5, -6, -7, -8]]])
        self.assertEqual(A.shape, (1, 2, 8))
        B = T.fit_transform(A)
        self.assertEqual(B.shape, (1, 4, 2, 2))
        diff = B == np.array([[[[1, 2], [-1, -2]], [[3, 4], [-3, -4]],
                               [[5, 6], [-5, -6]], [[7, 8], [-7, -8]]]])
        self.assertTrue(np.all(diff))

    def test_correlation(self):
        T = Correlation()
        A = np.array([[[[1, 2, 3, 4], [-1, -2, -3, -4]]]])
        B = T.fit_transform(A)
        diff = B == [[[-1, 0, 2]]]
        self.assertTrue(np.all(diff))

    def test_windower_and_correlation(self):
        T = Windower(2)
        T2 = Correlation()
        A = np.array([[[1, 2, 3, 4, 5, 6, 7, 8],
                       [-1, -2, -3, -4, -5, -6, -7, -8]]])
        P = Pipeline([
            ('windower', T),
            ('correlation', T2)
        ])
        B = P.fit_transform(A)
        diff = B == np.array([[[-1, 0, 2], [-1, 0, 2]]])
        self.assertTrue(np.all(diff))

    def test_feature_union(self):
        sys.argv = ['test', '-m', 'fext']
        P = FeatureUnion([
            ('pipeline1', Pipeline([
                ('windower', Windower(2)),
                ('correlation', Correlation()),
            ])),
            ('pipeline2', Pipeline([
                ('windower', Windower(2)),
                ('correlation', Correlation()),
            ])),
        ])
        A = np.array([[[1, 2, 3, 4, 5, 6, 7, 8],
                       [-1, -2, -3, -4, -5, -6, -7, -8]]])

        B = P.fit_transform(A)
        diff = B == np.array([[[-1, 0, 2, -1, 0, 2], [-1, 0, 2, -1, 0, 2]]])
        self.assertTrue(np.all(diff))

    def test_psd_sum_per_bin(self):
        T = PSDSumPerBin([0.5, 1.0, 2.0], 2)
        A = np.array([[[[1, 1, 1, 1, 1, 1, 1]]]], dtype=np.float32)
        B = T.fit_transform(A)
        diff = np.isclose(B, np.array([[[[3./7, 1./7]]]]))
        self.assertTrue(np.all(diff))

    def test_psd_sum_per_bin2(self):
        T = PSDSumPerBin([0.5, 1, 2, 4, 8], 16)
        A = np.array([[[np.sqrt([1, 2, 3, 4, 5, 6, 7, 8, 9]),
                        [1, 1, 1, 1, 1, 1, 1, 1, 1]]]], dtype=np.float32)
        B = T.fit_transform(A)
        diff = np.isclose(B, np.array([[[[1./45, 2./45, 7./45, 26./45],
                                         [1./9,  1./9,  2./9,  4./9]]]]))
        self.assertTrue(np.all(diff))

    def test_psd_sum_per_bin3(self):
        T = PSDSumPerBin([0.5, 1, 2, 4, 8], 16)
        A = np.array([[[np.sqrt([1, 2, 3, 4, 5, 6, 7, 8, 9]),
                        [1, 1, 1, 1, 1, 1, 1, 1, 1]],
                       [np.sqrt([1, 2, 3, 4, 5, 6, 7, 8, 9]),
                        [1, 1, 1, 1, 1, 1, 1, 1, 1]]]], dtype=np.float32)
        B = T.fit_transform(A)
        diff = np.isclose(B, np.array([[[[1./45, 2./45, 7./45, 26./45],
                                         [1./9,  1./9,  2./9,  4./9]],
                                         [[1./45, 2./45, 7./45, 26./45],
                                         [1./9,  1./9,  2./9,  4./9]]]]))
        self.assertTrue(np.all(diff))

    def test_shannon_entropy(self):
        T = ShannonEntropy()
        A = np.array([[[[1, 2, 4]]]])
        B = T.fit_transform(A)
        diff = np.isclose(B, np.array([[[[-10./np.log2(3)]]]]))
        self.assertTrue(np.all(diff))

    def test_psd_and_shannon(self):
        P = make_pipeline(PSDSumPerBin([0.5, 1, 2, 4, 8], 16), ShannonEntropy())
        A = np.array([[[np.sqrt([1, 2, 3, 4, 5, 6, 7, 8, 9]),
                        [1, 1, 1, 1, 1, 1, 1, 1, 1]],
                       [np.sqrt([1, 2, 3, 4, 5, 6, 7, 8, 9]),
                        [1, 1, 1, 1, 1, 1, 1, 1, 1]]]], dtype=np.float32)
        B = P.fit_transform(A)
        diff = np.isclose(B, np.array([[[0.59826438157551853, 0.8533],
                                        [0.59826438157551853, 0.8533]]]))
        self.assertTrue(np.all(diff))

class SlowTests(unittest.TestCase):
    def test_phase_synchrony(self):
        filename = "/Volumes/LESZCZU/eeg/pat_11502/adm_115102/rec_11500102/11500102_0000.data"
        head, data = read_data(filename)

        data.resize((1,) + data.shape)

        W = Windower(8)
        T1 = PhaseSynchrony()
        T2 = PhaseSynchronyMike()

        data = W.fit_transform(data)
        out1 = T1.fit_transform(data)

        out2 = T2.apply(data.reshape(data.shape[1:]), {})

        self.assertTrue(np.array_equal(out2, out1.reshape(out1.shape[1:])))



if __name__ == '__main__':
    unittest.main()
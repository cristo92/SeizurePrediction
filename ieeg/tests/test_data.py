import unittest

from ieeg.data import MetaData

class TestData(unittest.TestCase):
	def test_metadata(self):
		mdata = MetaData(11502)

		#equal
		self.assertTrue(mdata.was_seizure('2003-05-22 05:12:17.433594',
			                              '2003-05-22 05:13:15.054688'))
		# inside
		self.assertTrue(mdata.was_seizure('2003-05-22 05:12:57.433594',
			                              '2003-05-22 05:13:10.054688'))
		# left-cross
		self.assertTrue(mdata.was_seizure('2003-05-22 05:12:10.433594',
			                              '2003-05-22 05:13:00.054688'))
		# right-cross
		self.assertTrue(mdata.was_seizure('2003-05-22 05:12:47.433594',
			                              '2003-05-22 05:13:25.054688'))
		# left-ommit
		self.assertFalse(mdata.was_seizure('2003-05-22 05:12:10.433594',
			                               '2003-05-22 05:12:15.054688'))
		# right-ommit
		self.assertFalse(mdata.was_seizure('2003-05-22 05:13:17.433594',
			                               '2003-05-22 05:13:25.054688'))

		self.assertEqual(len(mdata.electrodes_set), 86)

	def test_metadata2(self):
		mdata = MetaData(109602)

		self.assertEqual(len(mdata.electrodes_set), 100)
		self.assertEqual(len(mdata.origin_set), 7)
		self.assertEqual(mdata.origin_set,
			             {'HL2', 'HL3', 'HL4', 'HL5', 'HL6', 'HL7', 'HL8'})

if __name__ == '__main__':
	unittest.main()
import unittest

import numpy as np

from ieeg.old_tasks.split_data import SplitData

class TestMainTasks(unittest.TestCase):
	def test_split_data(self):
		T = SplitData(None)
		D = T._run('ieeg/tests/features', 0.3)

		self.assertEqual(D['Y_train'].shape[0] + D['Y_test'].shape[0], 352 + 352)
		self.assertEqual(np.sum(D['Y_test']) + np.sum(D['Y_train']), 8 + 8)
		self.assertTrue(np.sum(D['Y_train']) > 0.7 * (8 + 8) - 1)
		self.assertTrue(np.sum(D['Y_test']) > 0.3 * (8 + 8) - 1)

if __name__ == "__main__":
	unittest.main()
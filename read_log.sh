#!/bin/bash

PPATH=""
LIST=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -w|--workflow)
    LIST+="workflow.py "
    shift # past argument
    ;;
    -d|--data_info)
    LIST+="data_info.txt "
    shift # past argument
    ;;
    -r|--raport)
    LIST+="raport.txt "
    shift # past argument
    ;;
    -p|--preds)
    LIST+="preds.csv "
    shift
    ;;
    -a|--all)
    LIST="workflow.py data_info.txt raport.txt"
    shift
    ;;
    *)    # unknown option
    PPATH="$1" # save it in an array for later
    shift # past argument
    ;;
esac
done


for FILE in $LIST
do
    cat `(ls -t $PPATH/info_*/$FILE | head -n 1)`
done


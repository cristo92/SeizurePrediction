[WIP] Seizure Prediction
==================

Project related to my master thesis - "Seizure prediction from EEG with 
Machine Learning". 

The code consists of the parts:
- Preprocessing  - in which I'm splitting 1,6TB EEG signals into 10 min chunks -
they are going to be used for training and validation of model.
- Signal selection - patients has 80 - 150 channels of EEG - we need to decrease 
that number and we need to standarize them - I'm using 4 different methods, 
which are going to be tested:
    - ICA
    - 20-10 EEG
    - k-meanns
    - window embedding
- Feature selection
- Classifier
- Validation

Data
----

There are two main metadata:

*recording* from database:

	- file_name :: str - name of file storing the recording
	- file_path :: str - path to folder storing file_name
	- block_recording :: int
	 
*seizure* recordings has as well:

	- seizure_id :: int
	- seizure_clin_onset :: str (format: `%Y-%m-%d %H:%M:%S.%f`) - timestamp of start of clinical seizure
	- seizure_clin_offset :: str - timestamp of end of clinical seizure
	- seizure_eeg_onset :: str - timestamp of start of eeg seizure i.e. it has been detected by eeg, not visually
	- seizure_eeg_offset :: str - timestamp of end of eeg seizure
	- seizure_vigilance :: str
	- seizure_classification :: str
	- seizure_pattern :: str
	- 
*head* file of the recording:

	- elec_names :: list - name of signals in the recording
	- adm_id :: int
	- sample_freq :: int - two main values are 256 and 1024
	- start_ts :: datetime.datetime - timestamp of start of recording
	- duration_in_sec :: int - length of recording in seconds
	- rec_id :: int
	- conversion_factor :: float
	- num_channels: int - number of signals in the recording (the same as len(elec_names))
	- num_samples: int - number of samples (equals `sample_freq` * `duration_in_sec`)
	- sample_bytes: int


Flow of data
------------

Data is generated into cache folder (from sys.argv).

```
/cache
	/signals -> hdf5 with chosen signals after preprocessing and signal selection
	/features -> hdf5 with features per patient
	/classifier.pkl -> trained classifier
	/raport.txt -> raport from feature extraction
	/predictions.csv -> predictions for data
```


Methods of standarazing EEG channels:
-------------------------------------

We're going to use N channels.

### ICA ###

I'm going to apply ICA, then choose N channels using [1].

### 20-10 EEG ###

As channels for prediction we choose surface channels i.e. 20-10 EEG, then
if we want decrise number of channels I will use one of the methods from [1].

### k-means ###

Every channel has its coordinates. For every patient I'm going to split them
into N kenels using k-mean method. From every kernel I'll choose the one channel 
using [1]. Now I need method to find good permutation of super channels for every 
patient.

### Window embedding. ###

For every patient I'm going to build auto-encoder. Auto-encoder will be a 
function f: R^(n x w) -> R^(N x w)


TODO
----

- [ ] Progressbar for preprocessing.

Bibliography:
=============

[1] 
import sqlite3

from ieeg.database import Database
from ieeg.data_info import get_patient_pids
from ieeg.parseHeader import read_head_from_seizure
from ieeg.dbQueries import get_seizures_by_patient, get_notseizures_by_patient
from ieeg.settings import parse_settings

if __name__ == "__main__":
	db = Database()

	try:
		for pid in parse_settings().pids:
			seizures1 = get_notseizures_by_patient(pid)
			seizures0 = get_seizures_by_patient(pid)

			print len(seizures1 + seizures0)

			for s in seizures0 + seizures1:
				try:
					head = read_head_from_seizure(s)
					head['file_id'] = s['file_id']
					db.add_header(**head)
				except sqlite3.IntegrityError:
					print head
	except IOError:
		pass	


	db.commit();

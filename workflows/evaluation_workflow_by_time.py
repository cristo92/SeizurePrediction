from . import *


workflow = Workflow({
    'global_params': {'args': parse_settings(sys.argv[1:]), 'pids': parse_settings().pids},
    'allow_metadata': {'metadata': parse_settings().metadata},
    'multi_evaluation': {
        "data_paths": ["autoencoder/all_patients/",],
        "labels": ["autoencoder"],
        "n_runs": 20,
        "test_size": 0.3,
        "valid_size": 0.3,
    },
}, "MainWorkflow") \
>> PrepareFolders('allow_metadata') \
>> MultiEvaluationByTime('multi_evaluation')

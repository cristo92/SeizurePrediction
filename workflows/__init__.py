import sys

import numpy as np
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.preprocessing import FunctionTransformer

from daftlearning.workflow import Workflow

from ieeg.settings import parse_settings
from ieeg.transformers import (Choose10_20Canals, ChooseHighestVariance, ica_transformer, Windower, Debbuger,
                               Correlation, FeatureUnion,
                               PSDSumPerBin, ShannonEntropy, Log10,
                               FlattenChannels, HFD, PFD, Hurst, Stats)
from ieeg.tasks.create_raport import CreateRaport
from ieeg.tasks.train_prods import TrainProds
from ieeg.tasks.cross_validation import CrossValidation
from ieeg.tasks.classifier import Classifier
from ieeg.tasks.xgboost_classifier import XGBoostClassifier
from ieeg.tasks.preprocessing_feature_extraction import PreprocessingFeatureExtraction
from ieeg.tasks.features_and_splitting import (ReadFeaturesAndSplit, ReadFeaturesAndSplitByPatient,
                                               ReadFeaturesAndSplitByPatientWithValid)
from ieeg.tasks.prepare_folders import PrepareFolders
from ieeg.tasks.save_result_database import SaveResultDatabase
from ieeg.tasks.fit_autoencoder import FitAutoencoder
from ieeg.tasks.multi_evaluation import MultiEvaluation
from ieeg.tasks.multi_evaluation_by_time import MultiEvaluationByTime
from ieeg.tasks.fit_shared_autoencoder import FitSharedAutoencoder
from ieeg.old_tasks.old import SaveAndReturn, LoadAndFeatureExtraction
from ieeg.old_tasks.preprocessing import PreprocessingTask
from ieeg.old_tasks.feature_extraction import FeatureExtraction
from ieeg.old_tasks.split_data import SplitData

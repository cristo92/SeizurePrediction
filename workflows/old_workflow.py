from . import *

def create_feature_pipeline(freq, windows):
    return FeatureUnion([
        ('Stats', Pipeline([
            ('windower', Windower(windows)),
            ('stats', Stats())
        ])),
        ('Correlation', Pipeline([
            ('windower', Windower(windows)),
            ('correlation', Correlation()),
        ])),
        ('FreqCorrelation', Pipeline([
            ('windower', Windower(windows)),
            ('FFT', FunctionTransformer(np.fft.rfft, validate=False)),
            ('Magnitude', FunctionTransformer(np.abs, validate=False)),
            ('correlation', Correlation()),
        ])),
        ('PowerInBand', Pipeline([
            ('windower', Windower(windows)),
            ('FFT', FunctionTransformer(np.fft.rfft, validate=False)),
            ('Magnitude', FunctionTransformer(np.abs, validate=False)),
            ('PowerSpectralDensity', PSDSumPerBin(
                [0.5, 2.25, 4, 5.5, 7, 9.5, 12, 21, 30, 39, 48],
                freq)),
            ('Log10', Log10()),
            ('FlattenChannels', FlattenChannels())
        ])),
        ('PowerInBandSpectralEntropy', Pipeline([
            ('windower', Windower(windows)),
            ('FFT', FunctionTransformer(np.fft.rfft, validate=False)),
            ('Magnitude', FunctionTransformer(np.abs, validate=False)),
            ('psd_with_shannon_entropy', FeatureUnion([
                make_pipeline(PSDSumPerBin(
                    (0.25, 1, 1.75, 2.5, 3.25, 4, 5, 8.5, 12, 15.5, 19.5, 24),
                    freq), ShannonEntropy()),
                make_pipeline(PSDSumPerBin([0.25, 2, 3.5, 6, 15, 24],
                                           freq),
                              ShannonEntropy()),
                make_pipeline(PSDSumPerBin(
                    [0.25, 2, 3.5, 6, 15],
                    freq), ShannonEntropy()),
                make_pipeline(PSDSumPerBin(
                    [0.25, 2, 3.5],
                    freq), ShannonEntropy()),
                make_pipeline(PSDSumPerBin(
                    [6, 15, 24],
                    freq), ShannonEntropy()),
                make_pipeline(PSDSumPerBin(
                    [2, 3.5, 6],
                    freq), ShannonEntropy()),
                make_pipeline(PSDSumPerBin(
                    [3.5, 6, 15],
                    freq), ShannonEntropy()),
            ])),
        ])),
        ('HFD', Pipeline([
            ('windower', Windower(windows)),
            ('hfd', HFD(2)),
        ])),
        ('PFD', Pipeline([
            ('windower', Windower(windows)),
            ('pfd', PFD()),
        ])),
        ('Hurst', Pipeline([
            ('windower', Windower(windows)),
            ('hurst', Hurst()),
        ]))
    ])

main_workflow = Workflow({
    'global_params': {'args': parse_settings(sys.argv[1:])},
    'preprocessing': {'args': parse_settings(sys.argv[1:]), 'batch_size': 16 * 8, 'observation_time': 75,
                      'margin_time': 20 * 60},
    'feature_extraction': {'args': parse_settings(sys.argv[1:]),
                           'feature_exctraction_pipeline': create_feature_pipeline(256, 8)},
    'split_data': {'args': parse_settings(sys.argv[1:]), 'test_size': 0.3},
    'classifier': {'args': parse_settings(sys.argv[1:]), 'gamma': 0.003, 'C': 1.0},
    'cross_validation': {'args': parse_settings(sys.argv[1:])},
}, "Workflow main.") \
>> PreprocessingTask('preprocessing') \
>> FeatureExtraction('feature_extraction') \
>> SplitData('split_data') \
>> Classifier('classifier') \
>> CrossValidation('cross_validation')